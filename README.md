**Color Blending::Photo blending with color and intensity**

Photos are good source of social components and shared a lot at social media. Photo is represented in different manners. Almost everyone wants to add some extra value by editing them before posting theme to social media such as *facebook*, *instagram*, *twitter* etc.

---

## About this repository

This bitbucket repository contains Xcode project on color blending over photos on iOS platform.

One can easily clone this repository with proper authentications.

1. Click **Source** on the left side.
2. Click the README.md link from the list of files.
3. Click the **Edit** button.
4. Delete the following text: *Delete this line to make a change to the README from Bitbucket.*
5. After making your change, click **Commit** and then **Commit** again in the dialog. The commit page will open and you’ll see the change you just made.
6. Go back to the **Source** page.

---

## Create a file

Next, you’ll add a new file to this repository.

1. Click the **New file** button at the top of the **Source** page.
2. Give the file a filename of **contributors.txt**.
3. Enter your name in the empty file space.
4. Click **Commit** and then **Commit** again in the dialog.
5. Go back to the **Source** page.

Before anyone move on, go ahead and explore the repository. You've already seen the **Source** page, but check out the **Commits**, **Branches**, and **Settings** pages.

---

## Project overview
