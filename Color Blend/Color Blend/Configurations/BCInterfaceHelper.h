//
//  BCInterfaceHelper.h
//  BucketCam
//
//  Created by Mostafizur Rahman on 5/28/17.
//  Copyright © 2017 Mostafizur Rahman. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>
#import <CoreGraphics/CoreGraphics.h>
#import "BCDefinedConstant.h"



@interface BCInterfaceHelper : NSObject

typedef void(^animationCopletion)(BOOL finished);
+(UIView *)getAdjustmentSliderView:(NSMutableArray *)filterDataArray
                            target:(id)a_target
                           perform:(SEL)selector
                           yOrigin:(const CGFloat)y_origin;
+(CALayer *)getLineLayer:(const CGRect)parentFrame;
+(void)updateFrameColorView:(const UIView *)colorView inSize:(const CGSize)imageSize;
+(void)animateOpacityViewToVisible:(UIView *)view_to_animate anim_completion:(animationCopletion)completion_block;
+(void)animateOpacityViewToInvisible:(UIView *)view_to_animate anim_completion:(animationCopletion)completion_block;
+(int)getSingleCharLength:(const UIFont *)font;
+(UIView *)getTextViewWithDelegate:(__weak id)delegate withFixedTextLength:(int *)textLength;

+(UIImageView*)getImageView:(NSString *)imageNamed p_rect:(const CGRect)rect;


+(CALayer *)getSubLayer:(const CGRect)subViewRext;
+(void)setRectBorader:(UIView *)view;
+(UIImage *)getRotatedImage:(UIImage *)source_image byRadian:(const double)rotation_angle_radian;
+(void)setLayerProperties:(const CALayer *)source_layer shouldRemove:(const BOOL)remove;
+(void)setDrawingButton:(UIButton *)drawingToolDoneButton;
+(UIImage *)getImage:(const CGSize)image_size withColor:(const UIColor *)src_color;
+(UIColor *)inverseColor:(const UIColor *)src_color;
+(CGFloat)getCircumscribedCircleRadius:(const CGSize)frameSize;
+(CGRect)getSnapViewRect:(const CGSize)imageSize topViewRect:(const CGRect)frame;
+(CGRect)getRoatatedRect:(const CGSize)imageSize;
+(void)setButtonLayer:(NSArray *)buttonCollections;

//fit image inside a view resides in a aprent view
+(CGRect)getViewRect:(const CGSize)image_size
             parentViewSize:(const CGSize)parent_view_size
          withPaddingOffset:(const CGFloat)offset;

@end
