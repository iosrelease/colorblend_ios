//
//  AppDelegate.h
//  Color Blend
//
//  Created by Mostafizur Rahman on 12/25/17.
//  Copyright © 2017 liilab. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

