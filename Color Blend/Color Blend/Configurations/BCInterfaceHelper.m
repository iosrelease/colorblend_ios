//
//  BCInterfaceHelper.m
//  BucketCam
//
//  Created by Mostafizur Rahman on 5/28/17.
//  Copyright © 2017 Mostafizur Rahman. All rights reserved.
//

#import "BCInterfaceHelper.h"
#import "BCBaseFilterData.h"
#import "CMDialogBase.h"

@implementation BCInterfaceHelper


+(CALayer *)getLineLayer:(const CGRect)parentFrame {
    CALayer *layer = [CALayer layer];
    layer.backgroundColor = [[UIColor blackColor] CGColor];
    layer.frame = CGRectMake(0, parentFrame.size.height - LINEWIDTH_HEADER,
                             parentFrame.size.width, LINEWIDTH_HEADER);
    return layer;
}

+(UIView *)getAdjustmentSliderView:(NSMutableArray *)filterDataArray
                            target:(id)a_target
                           perform:(SEL)selector
                           yOrigin:(const CGFloat)y_origin {
    
    const CGFloat height = 30 * [filterDataArray count] + 10;
    UIView *sliderHView = [[UIView alloc] initWithFrame:CGRectMake(0, y_origin, MAINSCRN_WIDTH, height)];
    sliderHView.tag = 9103;
    int origin_y = 5;
    const CGFloat l_width = MAINSCRN_WIDTH * 0.25;
    const CGFloat origin_x = 20 + l_width;
    const CGFloat s_width = MAINSCRN_WIDTH - origin_x - 10;
    for(BCBaseFilterData *b_data in filterDataArray){
        
        UILabel *sliderNameLabel = [[UILabel alloc] initWithFrame: CGRectMake(10, origin_y, l_width, 30)];
        sliderNameLabel.text = b_data._name;
        [sliderHView addSubview:sliderNameLabel];
        
        
        UISlider *f_slider = [[UISlider alloc] initWithFrame:CGRectMake(origin_x, origin_y, s_width, 30)];
        f_slider.restorationIdentifier = b_data._key;
        f_slider.value = b_data.default_value;
        f_slider.maximumValue = b_data.maximum_value;
        f_slider.minimumValue = b_data.minimun_value;
        f_slider.tintColor = THEME_UICOLOR;
        [f_slider addTarget:a_target action:selector forControlEvents:UIControlEventValueChanged];
        [sliderHView addSubview:f_slider];
        origin_y += 35;
    }
    
    return sliderHView;
}

+(void)setButtonLayer:(NSArray *)buttonCollections{
    for(UIButton *button in buttonCollections){
        button.layer.cornerRadius = button.frame.size.width / 2;
        button.layer.masksToBounds = YES;
    }
}

+(void)updateFrameColorView:(const UIView *)colorView inSize:(const CGSize)imageSize{
    const double ratio = imageSize.width / imageSize.height;
    const int viewTagArray[] = {MEDIATAG_IMAGE, MEDIATAG_EDTRVW, MEDIATAG_SRCIMG, MEDIATAG_SWPING};
    const double scrnRatio = 16.0 / 9.0;
    if(ratio > 1.0 || ratio < scrnRatio ){
        const CGFloat height = MAINSCRN_WIDTH / ratio;
        const CGFloat originY = (MAINSCRN_HEIGHT - height) / 2;
        const CGRect frame = CGRectMake(0, originY, MAINSCRN_WIDTH, height);
        for(int i = 0; i < 4; i++){
            UIView *view = [colorView viewWithTag:viewTagArray[i]];
            view.frame = frame;
        }
        
    } else {
        const CGFloat width = MAINSCRN_HEIGHT / ratio;
        const CGFloat originX = (MAINSCRN_WIDTH - width) / 2;
        const CGRect frame = CGRectMake(originX, 0, width, MAINSCRN_HEIGHT);
        for(int i = 0; i < 4; i++){
            UIView *view = [colorView viewWithTag:viewTagArray[i]];
            view.frame = frame;
        }
    }
}

+(void)animateOpacityViewToVisible:(UIView *)view_to_animate anim_completion:(animationCopletion)completion_block{
    view_to_animate.hidden = NO;
    view_to_animate.layer.opacity = 0.0f;
    view_to_animate.layer.transform = CATransform3DMakeScale(1.3f, 1.3f, 1.0);
    [UIView animateWithDuration:ANIMATION_DURATION delay:0.0 options:UIViewAnimationOptionCurveEaseInOut animations:^ {
         view_to_animate.layer.opacity = 1.0f;
         view_to_animate.layer.transform = CATransform3DMakeScale(1, 1, 1);
     } completion:^(BOOL finished) {
         completion_block(finished);
     }];
}
+(void)animateOpacityViewToInvisible:(UIView *)view_to_animate anim_completion:(animationCopletion)completion_block{
    CATransform3D currentTransform = view_to_animate.layer.transform;
    view_to_animate.layer.opacity = 1.0f;
    [UIView animateWithDuration:ANIMATION_DURATION delay:0.0 options:UIViewAnimationOptionTransitionNone animations:^ {
         view_to_animate.layer.transform = CATransform3DConcat(currentTransform, CATransform3DMakeScale(0.6f, 0.6f, 1.0));
         view_to_animate.layer.opacity = 0.0f;
     } completion:^(BOOL finished) {
         view_to_animate.hidden = YES;
         completion_block(finished);
     }];
}

+(int)getSingleCharLength:(const UIFont *)font {
    NSString *singleChar = @"A";
    CGSize stringBoundingBox = [singleChar sizeWithAttributes:@{@"Font":font}];
    return MAINSCRN_WIDTH / stringBoundingBox.width;
}

+(UIView *)getTextViewWithDelegate:(__weak id)delegate withFixedTextLength:(int *)textLength{
    
    CGSize p_retct = ((UIView *)delegate).frame.size;
    
    UIView *textViewContainer = [[UIView alloc] initWithFrame:CGRectMake(0, 0, p_retct.width, 30)];
    textViewContainer.userInteractionEnabled = YES;
    textViewContainer.backgroundColor = THEME_UICOLOR;
    textViewContainer.tag = VIEWTAG_EMOGADGET;
    textViewContainer.backgroundColor = [UIColor colorWithWhite:0.6f alpha:0.75f];
    UITextView *textView = [[UITextView alloc] initWithFrame:textViewContainer.bounds];
    [textView setText:@""];
    UIFont *textFont = [UIFont fontWithName:@"AppleSDGothicNeo-Regular" size:17];
    [textView setFont:textFont];
    
    [textView setTextColor:THEME_UICOLOR];
    textView.scrollEnabled = NO;
    textView.textAlignment = NSTextAlignmentCenter;
    textView.backgroundColor = [UIColor clearColor];
    textView.keyboardType = UITextAutocorrectionTypeYes;
    textView.delegate = delegate;
    
    [textView becomeFirstResponder];
    [textViewContainer addSubview:textView];
    *textLength = [BCInterfaceHelper getSingleCharLength:textFont];
    return textViewContainer;
}

+(UIImageView*)getImageView:(NSString *)imageNamed p_rect:(const CGRect)rect{
    UIImage *emoImage = [[UIImage alloc] initWithContentsOfFile:imageNamed];
    const float ratio = emoImage.size.width / emoImage.size.height;
    UIImageView *emoImageView = [[UIImageView alloc] initWithFrame:RANDOM_EMORECT(ratio, rect)];
    emoImageView.image = emoImage;
    emoImageView.userInteractionEnabled = YES;
    emoImageView.contentMode = UIViewContentModeScaleAspectFit;
    emoImageView.tag = VIEWTAG_EMOGADGET;
    return emoImageView;
}



+(CALayer *)getSubLayer:(const CGRect)subViewRext{
    CALayer *subLayer = [CALayer new];
    subLayer.borderColor = THEME_CGCOLOR;
    subLayer.borderWidth = 0.17f;
    subLayer.frame = subViewRext;
    return subLayer;
}


+(void)setRectBorader:(UIView *)view{
    view.layer.borderColor = [[UIColor blackColor] CGColor];
    view.layer.borderWidth  = 0.80f;
    view.layer.cornerRadius = 7;
    view.layer.masksToBounds = YES;
}

+(UIImage *)getRotatedImage:(UIImage *)source_image byRadian:(const double)rotation_angle_radian {
    BOOL isPieBy2 = M_PI_2 == fabs(rotation_angle_radian);
    const CGSize rotationImageSize =  isPieBy2 ? CGSizeMake(source_image.size.height, source_image.size.width) : source_image.size;
    const CGFloat rotation_center_x =  isPieBy2 ? rotationImageSize.width / 2 : -(rotationImageSize.width / 2) ;
    const CGFloat rotation_center_y =  isPieBy2 ? rotationImageSize.height / 2 : -(rotationImageSize.height / 2);
    UIGraphicsBeginImageContext(rotationImageSize);
    CGContextRef contextRef = UIGraphicsGetCurrentContext();
    CGContextSetShouldAntialias(contextRef, YES);
    if(isPieBy2){
        CGContextTranslateCTM(contextRef, rotation_center_x, rotation_center_y);
        CGContextRotateCTM(contextRef, rotation_angle_radian);
        CGContextTranslateCTM(contextRef, -rotation_center_y, -rotation_center_x);
        [source_image drawInRect:CGRectMake(0, 0, rotationImageSize.height, rotationImageSize.width)];
    } else {
        CGContextTranslateCTM(contextRef, -rotation_center_x, -rotation_center_y);
        CGContextRotateCTM(contextRef, rotation_angle_radian);
        CGContextTranslateCTM(contextRef, rotation_center_x, rotation_center_y);
        [source_image drawInRect:CGRectMake(0, 0, rotationImageSize.width, rotationImageSize.height)];
    }
    
    CGImageRef rotatedImageRef = CGBitmapContextCreateImage(contextRef);
    UIImage *outputImage = [[UIImage alloc] initWithCGImage:rotatedImageRef];
    CGImageRelease(rotatedImageRef);
    
    return outputImage;
}

+(void)setLayerProperties:(const CALayer*)source_layer shouldRemove:(const BOOL)remove{
    if(remove){
        source_layer.borderColor = [[UIColor clearColor] CGColor];
        source_layer.borderWidth = 0.0f;
        return;
    }
    source_layer.borderColor = THEME_CGCOLOR;
    source_layer.borderWidth = 0.75;
    
}

+(void)setDrawingButton:(UIButton *)drawingToolDoneButton {
    
    drawingToolDoneButton.layer.cornerRadius = 6;
    drawingToolDoneButton.layer.borderWidth = 1.25;
    drawingToolDoneButton.layer.borderColor = THEME_CGCOLOR;
    UIImage *src_colored_image = [BCInterfaceHelper getImage:drawingToolDoneButton.frame.size withColor:THEME_UICOLOR];
    [drawingToolDoneButton setBackgroundImage:src_colored_image forState:UIControlStateHighlighted];
    
}

+(UIImage *)getImage:(const CGSize)image_size withColor:(const UIColor *)src_color{
    UIGraphicsBeginImageContext(image_size);
    CGContextRef contextRef = UIGraphicsGetCurrentContext();
    
    CGContextSetShouldAntialias(contextRef, YES);
    CGContextSetFillColorWithColor(contextRef, src_color.CGColor);
    CGContextFillRect(contextRef, CGRectMake(0, 0, image_size.width, image_size.height));
    CGImageRef rotatedImageRef = CGBitmapContextCreateImage(contextRef);
    UIImage *outputImage = [[UIImage alloc] initWithCGImage:rotatedImageRef];
    CGImageRelease(rotatedImageRef);
    
    return outputImage;
}

+(UIColor *)inverseColor:(const UIColor *)src_color{
    CGColorRef src_colorRef = src_color.CGColor;
    const CGFloat *componentColors = CGColorGetComponents(src_colorRef);
//    CGColorRelease(src_colorRef);
    if(componentColors == NULL) return [UIColor whiteColor];
    return [UIColor colorWithRed:(1.0 - componentColors[0])
                                               green:(1.0 - componentColors[1])
                                                blue:(1.0 - componentColors[2])
                                               alpha:componentColors[3]];
    
}


+(CGFloat)getCircumscribedCircleRadius:(const CGSize)frameSize{
    const CGFloat width = frameSize.width;// + 20 * 2;
    const CGFloat height = frameSize.height;// + 20 * 2;
    const CGFloat arm_a = sqrt(width * width / 4 + height * height);
    const CGFloat arm_b = frameSize.width;
    const CGFloat radius = arm_a * arm_a * arm_b / sqrt((4 * arm_a * arm_a - arm_b * arm_b) * arm_b * arm_b);
    return radius;
}

+(CGRect)getSnapViewRect:(const CGSize)imageSize topViewRect:(const CGRect)frame{
    
    const CGFloat topContainerWidth = frame.size.height + frame.origin.y;
    const CGFloat imagViewHeight = MAINSCRN_HEIGHT - topContainerWidth;
    if(imageSize.width > imageSize.height){
        const CGFloat width = MAINSCRN_WIDTH - 20;
        const CGFloat height = width * imageSize.height / imageSize.width;
        const CGRect imageViewRect = CGRectMake(10, topContainerWidth + imagViewHeight / 2 -  height / 2, width, height);
        return imageViewRect;
    }
        const CGFloat height =  imagViewHeight - 20 ;
        const CGFloat width = height * imageSize.width / imageSize.height;
        const CGRect imageViewRect = CGRectMake(MAINSCRN_WIDTH / 2 - width / 2, topContainerWidth + 10, width, height);
     return imageViewRect;
}

+(CGRect)getRoatatedRect:(const CGSize)imageSize{
    const float ratio = imageSize.height / imageSize.width;
    if(ratio < MAINSCRN_RATIO){
        const CGFloat origin_x = 0;
        const CGFloat width = MAINSCRN_WIDTH;
        const CGFloat height = width * ratio;
        const CGFloat origin_y = (HALFMNSCRN_HEIGHT - height / 2) / 2;
        return CGRectMake(origin_x, origin_y, width, height);
    }
        const CGFloat origin_y = 0; 
        const CGFloat height = MAINSCRN_HEIGHT;
        const CGFloat width = height / ratio;
        const CGFloat origin_x = (HALFMNSCRN_WIDTH - width / 2) / 2;
        return CGRectMake(origin_x, origin_y, width, height);
}

+(CGRect)getViewRect:(const CGSize)image_size
             parentViewSize:(const CGSize)parent_view_size
          withPaddingOffset:(const CGFloat)offset {
    CGRect color_pop_rect;
    const CGFloat image_ratio = image_size.width / image_size.height;
    if(image_ratio < 1.f){
        CGFloat origin_y = offset;
        CGFloat height = parent_view_size.height - offset * 2.f;
        CGFloat width = height * image_ratio;
        if(width > parent_view_size.width - 2 * offset){
            width = parent_view_size.width - 2 * offset;
            height = width / image_ratio;
            origin_y = parent_view_size.height / 2 - height / 2;
            color_pop_rect = CGRectMake(offset, origin_y, width, height);
        } else {
            const CGFloat origin_x = parent_view_size.width / 2 - width / 2;
            color_pop_rect = CGRectMake(origin_x, origin_y, width, height);
        }
    } else {
        CGFloat origin_x = offset;
        CGFloat width = parent_view_size.width - offset * 2.f;
        CGFloat height = width / image_ratio;
        if(height > parent_view_size.height - 2 * offset){
            height = parent_view_size.height - 2 * offset;
            width = height * image_ratio;
            origin_x = parent_view_size.width / 2 - width / 2;
            color_pop_rect = CGRectMake(origin_x, offset, width, height);
        } else {
            const CGFloat origin_y = parent_view_size.height / 2 - height / 2;
            color_pop_rect = CGRectMake(origin_x, origin_y, width, height);
        }
    }
    return color_pop_rect;
    
}

//-(void)openDiloag:(const int)dtype parentView:(__weak UIViewController *)rootViewController message:(NSString *)message title:(NSString *)tittle image:(UIImage *)sourceImage {
//    CMDialogBase *baseD_v = [[CMDialogBase alloc] initWithDialogType:1];
//    baseD_v.transform = CGAffineTransformIdentity;
//    baseD_v.frame = [[UIScreen mainScreen] bounds];
//    baseD_v.hidden = YES;
//    [baseD_v setTitle:tittle];
//    [baseD_v setMessageBody:message];
//
//    [baseD_v setRootViewController:rootViewController];
//    [baseD_v setAddView:self.nativeExpressAdView];
//    if(MAINSCRN_WIDTH > 320){
//        [self.nativeExpressAdView loadRequest:request];
//    } else {
//        [baseD_v setImage:[self.glk_drawingView getOutputImage]];
//    }
//    [self.view addSubview:baseD_v];
//    
//    [BCInterfaceHelper animateOpacityViewToVisible:baseD_v anim_completion:^(BOOL finished) {
//        
//    }];
//}


@end
