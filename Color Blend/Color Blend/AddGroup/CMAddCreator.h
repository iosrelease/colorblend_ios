//
//  CMAddCreator.h
//  ColorMagic
//
//  Created by Mostafizur Rahman on 11/29/17.
//  Copyright © 2017 Mostafizur Rahman. All rights reserved.
//

#import <Foundation/Foundation.h>
@import GoogleMobileAds;
@interface CMAddCreator : NSObject
-(instancetype)initWithBanner:(GADBannerView *)bannerView;
@end
