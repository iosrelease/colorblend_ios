//
//  CMAddCreator.m
//  ColorMagic
//
//  Created by Mostafizur Rahman on 11/29/17.
//  Copyright © 2017 Mostafizur Rahman. All rights reserved.
//

#import "CMAddCreator.h"
#import "BCDefinedConstant.h"

@implementation CMAddCreator

-(instancetype)initWithBanner:(GADBannerView* )bannerView{
    self = [super init];
    [self createBannerAdd:bannerView];
    return self;
}

-(void)createBannerAdd:(GADBannerView*)bannerView{
    
#ifdef DEBUG
    // Something to log your sensitive data here
    bannerView.adUnitID = ADD_UNITID_BANNERVIEW;
#else
    bannerView.adUnitID = AD_BANNER_RELEASE;
    //
#endif
    
    
    GADRequest *request = [GADRequest request];
    // Requests test ads on devices you specify. Your test device ID is printed to the console when
    // an ad request is made. GADBannerView automatically returns test ads when running on a
    // simulator.
    request.testDevices = @[ kGADSimulatorID ];
    [bannerView loadRequest:request];
}

@end
