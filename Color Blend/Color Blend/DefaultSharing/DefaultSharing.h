//
//  DefaultSharing.h
//  FacebookShare
//
//  Created by Mamunul on 3/1/17.
//  Copyright © 2017 Mamunul. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>
#import "BCDefinedConstant.h"

@interface DefaultSharing : NSObject<UIDocumentInteractionControllerDelegate>

-(void)shareImage:(UIImage *)image fromViewController:(UIViewController*)viewController socilaMedaiType:(CMShareMediaType)s_mediaType;

-(void)shareVideo:(NSURL *)videoUrl fromViewController:(UIViewController*)viewController socilaMedaiType:(CMShareMediaType)s_mediaType;

@end
