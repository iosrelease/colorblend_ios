//
//  CMShareViewController.m
//  ColorMagic
//
//  Created by Mostafizur Rahman on 11/28/17.
//  Copyright © 2017 Mostafizur Rahman. All rights reserved.
//

#import "CMShareViewController.h"
#import "BCDefinedConstant.h"
#import "BCInterfaceHelper.h"
#import "DefaultSharing.h"
#import "CMDialogBase.h"


@interface CMShareViewController ()<
GADNativeExpressAdViewDelegate,
GADVideoControllerDelegate>{
    CMAddCreator *add_creator;
    DefaultSharing *defaultSharing;
}

@end

@implementation CMShareViewController
static NSString *const AdUnitId = @"ca-app-pub-3940256099942544/8897359316";
- (void)viewDidLoad {
    [super viewDidLoad];
    defaultSharing = [[DefaultSharing alloc] init];
    __weak CMShareViewController *self_vc = self;
    self.add_bannerView.rootViewController = self_vc;
    add_creator = [[CMAddCreator alloc] initWithBanner:self.add_bannerView];
    
    
    // Do any additional setup after loading the view.
}

-(void)dealloc{
    self.add_bannerView.rootViewController = nil;
    add_creator = nil;
}

-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    self.shareButton.layer.cornerRadius = self.shareButton.frame.size.width / 2;
    self.shareButton.layer.masksToBounds = YES;
    self.shareButton.layer.borderColor = [UIColor blackColor].CGColor;
    self.shareButton.layer.borderWidth = 5;
    self.saveImageView.image = self.sourceImage;
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)shareImage:(UIButton *)sender {
    if([sender tag] == CMShareMediaFacebook){
        [defaultSharing shareImage:self.sourceImage fromViewController:self socilaMedaiType:CMShareMediaFacebook];
    } else if([sender tag] == CMShareMediaInstagram){
        [defaultSharing shareImage:self.sourceImage fromViewController:self socilaMedaiType:CMShareMediaInstagram];
    } else if([sender tag] == CMShareMediaTwitter){
        [defaultSharing shareImage:self.sourceImage fromViewController:self socilaMedaiType:CMShareMediaTwitter];
    } else if([sender tag] == CMShareMediaMore){
        [defaultSharing shareImage:self.sourceImage fromViewController:self socilaMedaiType:CMShareMediaMore];
    }
}

-(void)openDialog:(NSString *)title message:(NSString *)message {
    dispatch_async(dispatch_get_main_queue(), ^{
        CMDialogBase *baseD_v = [[CMDialogBase alloc] initWithDialogType:1];
        baseD_v.transform = CGAffineTransformIdentity;
        baseD_v.frame = [[UIScreen mainScreen] bounds];
        baseD_v.hidden = YES;
        [baseD_v setTitle:title];
        [baseD_v setMessageBody:message];
        __weak CMShareViewController *self_vc = self;
        [baseD_v setRootViewController:self_vc];        
        [baseD_v setImage:self.sourceImage];
        [self.view addSubview:baseD_v];
        [BCInterfaceHelper animateOpacityViewToVisible:baseD_v anim_completion:^(BOOL finished) {
            
        }];
    });
}

-(void)clicked_Okay{
    NSLog(@"okay");
}

- (IBAction)rateUs:(id)sender {
}

- (IBAction)exitSharing:(id)sender {
    self.sourceImage = nil;
    [self.navigationController popViewControllerAnimated:YES];
}

- (IBAction)openAppStore:(id)sender {
    NSString *appName = [NSString stringWithString:[[[NSBundle mainBundle] infoDictionary]   objectForKey:@"CFBundleName"]];
    NSURL *appStoreURL = [NSURL URLWithString:[NSString stringWithFormat:@"itms-apps://itunes.com/app/%@",
                                               [appName stringByReplacingOccurrencesOfString:@" " withString:@""]]];
    [[UIApplication sharedApplication] openURL:appStoreURL];
}

@end
