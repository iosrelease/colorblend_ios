//
//  CMShareViewController.h
//  ColorMagic
//
//  Created by Mostafizur Rahman on 11/28/17.
//  Copyright © 2017 Mostafizur Rahman. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CMAddCreator.h"

@interface CMShareViewController : UIViewController
@property (weak, nonatomic) IBOutlet UIButton *shareButton;

@property (weak, nonatomic) IBOutlet UIView *imageContainerView;

@property (weak, nonatomic) IBOutlet UIImageView *saveImageView;

@property (weak, nonatomic) IBOutlet UIView *buttonContainerView;

@property (readwrite) UIImage *sourceImage;
@property (weak, nonatomic) IBOutlet GADBannerView *add_bannerView;

-(void)openDialog:(NSString *)title message:(NSString *)message;
@end
