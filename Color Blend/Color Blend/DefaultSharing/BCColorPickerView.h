//
//  BCColorPickerView.h
//  BucketCam
//
//  Created by Mostafizur Rahman on 6/18/17.
//  Copyright © 2017 Mostafizur Rahman. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BCPixelImageView.h"


@protocol ColorSelectionDelegate <NSObject>

-(void)didSelectColor:(UIColor *)selectedColor;

@end

@interface BCColorPickerView : UIView


@property (readwrite) BOOL shouldChooseColor;
@property (readwrite, weak) BCPixelImageView *__colorCircleView;


@property (readwrite, strong) id<ColorSelectionDelegate> colorSelectionDelegate;
@end
