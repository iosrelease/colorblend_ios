//
//  BCColorPickerView.m
//  BucketCam
//
//  Created by Mostafizur Rahman on 6/18/17.
//  Copyright © 2017 Mostafizur Rahman. All rights reserved.
//

#import "BCColorPickerView.h"
#import "BCInterfaceHelper.h"
#import "BCDefinedConstant.h"

@interface BCColorPickerView()<UIGestureRecognizerDelegate>{
    CGPoint relativeCenter;
    
    
    __weak UIImageView *__indicatorView;
    UIColor *__selectedColor;
    UIView *draggingView;
    UIPanGestureRecognizer *__panGesture;
    UITapGestureRecognizer *__tapGesture;
}

@end

@implementation BCColorPickerView
@synthesize colorSelectionDelegate, __colorCircleView;

-(instancetype)initWithCoder:(NSCoder *)aDecoder{
    self = [super initWithCoder:aDecoder];
    [self initializeSubviews];
    return self;
}

-(void)initializeSubviews{
    _shouldChooseColor = YES;
    __colorCircleView = [self viewWithTag:MEDIATAG_COLORCIRCLE];
    __indicatorView = [self viewWithTag:MEDIATAG_INDICATOR];
    draggingView = __indicatorView;
    __panGesture = [[UIPanGestureRecognizer alloc] initWithTarget:self action:@selector(recognizePanGesture:)];
    __panGesture.delegate = self;
    [__indicatorView addGestureRecognizer:__panGesture];
    
//    __tapGesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(recognizeTapGesture:)];
//    __tapGesture.numberOfTapsRequired = 1;
//    [self addGestureRecognizer:__tapGesture];
}

-(void)dealloc{
    [__colorCircleView freeMemory];
    [__indicatorView removeGestureRecognizer:__panGesture];
    [__panGesture removeTarget:self action:@selector(recognizePanGesture:)];
    __panGesture.delegate = nil;
    __panGesture = nil;
    
//    [self removeGestureRecognizer:__tapGesture];
//    [__tapGesture removeTarget:self action:@selector(recognizeTapGesture:)];
//    __tapGesture.delegate = nil;
//    __tapGesture = nil;
    
    //free allocated colored memory
    
}

-(void)touchesBegan:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event{
    [super touchesBegan:touches withEvent:event];
    const CGPoint touchedPoint = [[touches allObjects ].firstObject locationInView:self];
    __indicatorView.center = touchedPoint;
    const CGPoint colorPoint = [self convertPoint:__indicatorView.center
                                           toView:__colorCircleView];
    __selectedColor = [__colorCircleView getRGBPixelColorAtPoint:colorPoint];
    [colorSelectionDelegate didSelectColor:__selectedColor];
    self.backgroundColor = __selectedColor;
    self.alpha = 1;
}

-(void)touchesEnded:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event{
    [super touchesBegan:touches withEvent:event];
    self.alpha = 0.2;
}


-(BOOL)gestureRecognizerShouldBegin:(UIGestureRecognizer *)gestureRecognizer{
    self.shouldChooseColor = YES;
    return self.shouldChooseColor;
}


-(void)recognizeTapGesture:(UIPanGestureRecognizer *)tapGesture{
    
    
    if(tapGesture.state == UIGestureRecognizerStateBegan){
        self.alpha = 1.0;
    } else{
        self.alpha = 0.20;
    }
    
    
    
}

-(void)recognizePanGesture:(UIPanGestureRecognizer *)panGesture{
    
    
    const CGPoint colorPoint = [self convertPoint:__indicatorView.center
                                           toView:__colorCircleView];
    __selectedColor = [__colorCircleView getRGBPixelColorAtPoint:colorPoint];
    [colorSelectionDelegate didSelectColor:__selectedColor];
    self.backgroundColor = __selectedColor;
    if(panGesture.state == UIGestureRecognizerStateBegan){
        self.alpha = 1.0;
        const CGPoint touchedPoint = [panGesture locationInView:__indicatorView];
        const float originX = (__indicatorView.frame.size.width / 2) - touchedPoint.x;
        const float originY = (__indicatorView.frame.size.height / 2) - touchedPoint.y + 5 * 2; // make this static value to dynamic
        relativeCenter = CGPointMake(originX, originY);
    } else if(panGesture.state == UIGestureRecognizerStateChanged){
        if(self.tag == MEDIATAG_COLORPICKER){
            const CGPoint touchedPoint = [panGesture locationInView:self];
            panGesture.view.center =  CGPointMake(touchedPoint.x + relativeCenter.x,
                                                  touchedPoint.y + relativeCenter.y);
        } else {
            const CGPoint touchedPoint = [panGesture locationInView:self];
            panGesture.view.center =  CGPointMake(touchedPoint.x + relativeCenter.x,
                                                  self.bounds.size.height / 2);
        }
    } else {
        //        ended or canceled
        self.alpha = .2;
        if (SHOULD_ANIMATE_DRAGVIEW) {
            
            [UIView animateWithDuration:ANIMATION_DURATION animations:^{
                draggingView.center = DRAGVIEW_CENTERPOINT;
                
            }];
        }
        self.shouldChooseColor = NO;
    }
}
@end
