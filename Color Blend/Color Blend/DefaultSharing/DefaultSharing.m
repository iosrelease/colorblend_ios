//
//  DefaultSharing.m
//  FacebookShare
//
//  Created by Mamunul on 3/1/17.
//  Copyright © 2017 Mamunul. All rights reserved.
//

#import "DefaultSharing.h"
#import <Social/Social.h>
#import "CMShareViewController.h"

@interface DefaultSharing(){
    UIDocumentInteractionController *documentController;
    __weak UIViewController *rootVC;
}
@end
@implementation DefaultSharing

-(void)dealloc{
    rootVC = nil;
    documentController.delegate = nil;
    documentController = nil;
}

-(void)shareImage:(UIImage *)image fromViewController:(UIViewController *)viewController socilaMedaiType:(CMShareMediaType)s_mediaType{

	NSArray *activityItems = @[image];
	[self shareItem:activityItems fromViewController:viewController socilaMedaiType:s_mediaType];

}


-(void)shareVideo:(NSURL *)videoUrl fromViewController:(UIViewController *)viewController socilaMedaiType:(CMShareMediaType)s_mediaType{

	NSArray *activityItems = @[videoUrl];
	[self shareItem:activityItems fromViewController:viewController socilaMedaiType:(CMShareMediaType)s_mediaType];

}


- (void)shareItem:(NSArray *)item fromViewController:(UIViewController *)viewController socilaMedaiType:(CMShareMediaType)s_mediaType {
    if(s_mediaType == CMShareMediaFacebook){
        [self shareAtTwitter:(NSArray *)item atViewController:(UIViewController *)viewController socilaType:SLServiceTypeFacebook];
    } else if(s_mediaType == CMShareMediaTwitter){
        [self shareAtTwitter:(NSArray *)item atViewController:(UIViewController *)viewController socilaType:SLServiceTypeTwitter];
    } else if(s_mediaType == CMShareMediaInstagram){
            [self shareAtInstagram:item atViewController:viewController];

    } else if(s_mediaType == CMShareMediaMore){
        [self shareMediaAtAllSites:item atViewController:viewController];
    }
   /* */

	
}

-(void)shareAtTwitter:(NSArray *)item atViewController:(UIViewController *)viewController socilaType:(NSString *)str{
    rootVC = viewController;
    NSString *lastObject = [[str componentsSeparatedByString:@"."] lastObject];
    NSString *appName = [lastObject isEqualToString:@"facebook"] ?
    @"fb" : @"twitter";
    appName = [NSString stringWithFormat:@"%@://app",appName];
    NSURL *url = [NSURL URLWithString:appName];
    if([[UIApplication sharedApplication] canOpenURL:url]){
        SLComposeViewController *fbSLComposeViewController = [SLComposeViewController composeViewControllerForServiceType:str];
        if (fbSLComposeViewController != nil) {
            
            [fbSLComposeViewController setInitialText:@"#BlendColor"];
            [fbSLComposeViewController addImage:item.firstObject];
            [viewController presentViewController:fbSLComposeViewController animated:YES completion:nil];
            
            fbSLComposeViewController.completionHandler = ^(SLComposeViewControllerResult result) {
                switch(result) {
                    case SLComposeViewControllerResultCancelled:
                        [self showAlertController:[NSString stringWithFormat:@"%@ Sharing Canceled",lastObject]
                                      withMessage:[NSString stringWithFormat:@"%@ sharing aborted or %@ account is inaccessible. Please! Check %@ app.", lastObject,lastObject,lastObject]
                                           rootVC:rootVC];
                        break;
                    case SLComposeViewControllerResultDone:
                        [self showAlertController:@"Facebook Sharing"
                                      withMessage:@"Photo will be shared on facebook soon! It will take few seconds."
                                           rootVC:rootVC];
                        
                        break;
                }
            };
        }
    }
   
    else {
        NSString *serviceName = [str containsString:@"facebook"] ? @"facebook" :
        [str containsString:@"twitter"] ? @"twitter" : @"Requested Service";
        NSString *title = [NSString stringWithFormat:@"%@ Unavailable", serviceName];
        NSString *message = [NSString stringWithFormat:@"Sorry, we're unable to find a %@ account on your device."
                             " Please login  and try again.", serviceName];
        [self showAlertController:title withMessage:message rootVC:viewController];
        
        
        
    }
}


-(void)showAlertController:(NSString *)title withMessage:(NSString *)message rootVC:(UIViewController *)viewController{
    CMShareViewController *shareViewController = (CMShareViewController *)viewController;
    [shareViewController openDialog:title message:message];
}


-(void)shareAtInstagram:(NSArray *)item atViewController:(UIViewController *)viewController {
    UIImage *image = item[0];
    
    //Add this line of code instead of doing step One if you are at early version of iOS9 till iOS 9.2.3  but if you are using iOS9.5 or above like as iOS10 not use this line and do step1 instead of writing this line of code
    NSURL *instagram = [NSURL URLWithString:@"instagram://app"];
    
    
    
    if ([[UIApplication sharedApplication] canOpenURL:instagram]) {
        
        //convert image into .png format.
        NSData *imageData = UIImagePNGRepresentation(image);
        
        //create instance of NSFileManager
        NSFileManager *fileManager = [NSFileManager defaultManager];
        
        //create an array and store result of our search for the documents directory in it
        NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
        
        //create NSString object, that holds our exact path to the documents directory
        NSString *documentsDirectory = [paths objectAtIndex:0];
        
        //add our image to the path
        NSString *fullPath = [documentsDirectory stringByAppendingPathComponent:[NSString stringWithFormat:@"insta.igo"]];
        
        //finally save the path (image)
        [fileManager createFileAtPath:fullPath contents:imageData attributes:nil];
        
        CGRect rect = CGRectMake(0 ,0 , 0, 0);
        UIGraphicsBeginImageContextWithOptions(viewController.view.bounds.size, viewController.view.opaque, 0.0);
        [viewController.view.layer renderInContext:UIGraphicsGetCurrentContext()];
        UIGraphicsEndImageContext();
        
        NSString *fileNameToSave = [NSString stringWithFormat:@"Documents/insta.igo"];
        NSString *jpgPath = [NSHomeDirectory() stringByAppendingPathComponent:fileNameToSave];
        
        NSString *newJpgPath = [NSString stringWithFormat:@"file://%@",jpgPath];
        
        // NSURL *igImageHookFile = [[NSURL alloc]initFileURLWithPath:newJpgPath];
        
        NSURL *igImageHookFile = [NSURL URLWithString:newJpgPath];
        if(documentController){
            documentController.delegate = nil;
        }
        documentController = [UIDocumentInteractionController interactionControllerWithURL:igImageHookFile ];
        documentController.delegate = self;
        [documentController setUTI:@"com.instagram.exclusivegram"];
        [documentController presentOpenInMenuFromRect:rect inView:viewController.view animated:YES];
        
        
    } else {
        // NSLog (@"Instagram not found");
        [self showAlertController:@"Install Instagram"
                      withMessage:@"Before Sharing to Instagram, Please Install Instagram app to your Device."
                           rootVC:viewController];
        
    }
}

-(void)shareMediaAtAllSites:(NSArray *)item atViewController:(UIViewController *)viewController{
    UIActivityViewController *activityVC = [[UIActivityViewController alloc] initWithActivityItems:item applicationActivities:nil];
    activityVC.excludedActivityTypes = @[UIActivityTypeAssignToContact,
                                         UIActivityTypePrint,
                                         UIActivityTypePostToWeibo,
                                         UIActivityTypeMessage,
                                         UIActivityTypeMail,
                                         UIActivityTypeCopyToPasteboard,
                                         UIActivityTypeAddToReadingList,
                                         UIActivityTypePostToTencentWeibo,
                                         UIActivityTypePostToFlickr,
                                         @"com.apple.mobilenotes.SharingExtension",
                                         @"com.apple.mobileslideshow.StreamShareService",
                                         @"com.apple.reminders.RemindersEditorExtension",
                                         ];
    activityVC.completionWithItemsHandler = ^(NSString *activityType, BOOL completed, NSArray *returnedItems, NSError *activityError) {
        if(activityType != nil){
            if (completed)
            {
                [self showAlertController:@"Task Success!"
                              withMessage:@"Requested operation is completed successfully!"
                                   rootVC:viewController];
                
            }
            else
            {
                [self showAlertController:@"Don't worry!"
                              withMessage:@"Requested operation is aborted!"
                                   rootVC:viewController];
            }
        }
    };
    if ( [activityVC respondsToSelector:@selector(popoverPresentationController)] ) {
        // iPad support
        activityVC.popoverPresentationController.sourceView = viewController.view;
    }
    [viewController presentViewController:activityVC animated:YES completion:nil];
}

//// If preview is supported, this provides the view controller on which the preview will be presented.
//// This method is required if preview is supported.
//// If presenting atop a navigation stack, provide the navigation controller in order to animate in a manner consistent with the rest of the platform.
//- (UIViewController *)documentInteractionControllerViewControllerForPreview:(UIDocumentInteractionController *)controller{
//    NSLog(@"Fired dc delegate");
//    
//}
//
//// If preview is supported, these provide the view and rect that will be used as the starting point for the animation to the full screen preview.
//// The actual animation that is performed depends upon the platform and other factors.
//// If documentInteractionControllerRectForPreview is not implemented, the specified view's bounds will be used.
//// If documentInteractionControllerViewForPreview is not implemented, the preview controller will simply fade in instead of scaling up.
//- (CGRect)documentInteractionControllerRectForPreview:(UIDocumentInteractionController *)controller{
//    NSLog(@"Fired dc delegate");
//}
//- (nullable UIView *)documentInteractionControllerViewForPreview:(UIDocumentInteractionController *)controller{
//    NSLog(@"Fired dc delegate");
//}

// Preview presented/dismissed on document.  Use to set up any HI underneath.
- (void)documentInteractionControllerWillBeginPreview:(UIDocumentInteractionController *)controller{
}
- (void)documentInteractionControllerDidEndPreview:(UIDocumentInteractionController *)controller{
}

// Options menu presented/dismissed on document.  Use to set up any HI underneath.
- (void)documentInteractionControllerWillPresentOptionsMenu:(UIDocumentInteractionController *)controller{
}
- (void)documentInteractionControllerDidDismissOptionsMenu:(UIDocumentInteractionController *)controller{
}

// Open in menu presented/dismissed on document.  Use to set up any HI underneath.
- (void)documentInteractionControllerWillPresentOpenInMenu:(UIDocumentInteractionController *)controller{
}
- (void)documentInteractionControllerDidDismissOpenInMenu:(UIDocumentInteractionController *)controller{
    documentController.delegate = nil;
    documentController = nil;
}

// Synchronous.  May be called when inside preview.  Usually followed by app termination.  Can use willBegin... to set annotation.
- (void)documentInteractionController:(UIDocumentInteractionController *)controller willBeginSendingToApplication:(nullable NSString *)application
{
}// bundle ID
- (void)documentInteractionController:(UIDocumentInteractionController *)controller didEndSendingToApplication:(nullable NSString *)application{
    documentController.delegate = nil;
    documentController = nil;
}


@end
