//
//  BCPixelImageView.h
//  BucketCam
//
//  Created by Mostafizur Rahman on 9/13/17.
//  Copyright © 2017 Mostafizur Rahman. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface BCPixelImageView : UIImageView

-(void)setScale;
-(BOOL)freeMemory;
- (UIColor*)getRGBPixelColorAtPoint:(CGPoint)point;
-(void)setPixels;
@end
