//
//  BCPixelImageView.m
//  BucketCam
//
//  Created by Mostafizur Rahman on 9/13/17.
//  Copyright © 2017 Mostafizur Rahman. All rights reserved.
//

#import "BCPixelImageView.h"

@interface BCPixelImageView (){
    double x_scale;
    double y_scale;
    unsigned long height;
    unsigned long width;
    unsigned char *pixel_rgba;
}

@end


@implementation BCPixelImageView

-(instancetype)initWithCoder:(NSCoder *)aDecoder{
    self = [super initWithCoder:aDecoder];
    [self setPixels];
    return self;
}

-(void)setImage:(UIImage *)image{
    [super setImage:image];
    [self setPixels];
}


-(void)setPixels{
    
   
    [self freeMemory];
    CGImageRef cgImage = CGImageRetain([self.image CGImage]);
    width = CGImageGetWidth(cgImage);
    height = CGImageGetHeight(cgImage);
    x_scale = width / self.frame.size.width;
    y_scale = height / self.frame.size.height;
    
    const long d_len = width * height * 4;
    pixel_rgba = malloc(d_len);
    [self setScale];
    CGDataProviderRef providerRef = CGImageGetDataProvider(cgImage);
    CFDataRef bitmapDataRef = CGDataProviderCopyData(providerRef);
    const unsigned char *data = (unsigned char *)CFDataGetBytePtr(bitmapDataRef);
    memcpy(pixel_rgba, data, d_len);
    CGImageRelease(cgImage);
    CFRelease(bitmapDataRef);
}

-(void)setScale{
}



- (UIColor*)getRGBPixelColorAtPoint:(CGPoint)point {
    UIColor* color = [UIColor blackColor];
    NSUInteger x = (NSUInteger)floor(point.x) * x_scale;
    NSUInteger y = (NSUInteger)floor(point.y) * y_scale ;
    if ((x < width) && (y < height))
    {
        size_t offset = ((width * y) + x) * 4;
        int alpha = pixel_rgba[offset + 3];
        
#if TARGET_IPHONE_SIMULATOR
        int red =   pixel_rgba[offset];
        int green = pixel_rgba[offset + 1];
        int blue =  pixel_rgba[offset + 2];
#else
        //on device
        int blue =  pixel_rgba[offset];       //notice red and blue are swapped
        int green = pixel_rgba[offset + 1];
        int red =   pixel_rgba[offset + 2];
#endif
        
        color = [UIColor colorWithRed:blue/255.0f green:green/255.0f blue:red/255.0f alpha:alpha/255.0f];
    }
    return color;
}

-(BOOL)freeMemory {
    if(pixel_rgba != NULL){
        free(pixel_rgba);
        pixel_rgba = NULL;
    }
    return pixel_rgba == NULL;
    
}

-(void)dealloc{
    
}

@end
