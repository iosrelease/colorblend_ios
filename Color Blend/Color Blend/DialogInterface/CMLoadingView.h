//
//  CMLoadingView.h
//  ColorMagic
//
//  Created by Mostafizur Rahman on 11/22/17.
//  Copyright © 2017 Mostafizur Rahman. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CMLoadingView : UIVisualEffectView
+(BOOL)isLoading;
+(void)addVisualEffectOnView:(UIView *)parentView;
+(void)removeVisulaEffectFromView:(UIView *)parentView;
+(instancetype)sharedLoadingView;
@end
