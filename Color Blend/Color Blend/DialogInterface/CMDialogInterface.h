//
//  CMDialogInterface.h
//  ColorMagic
//
//  Created by Mostafizur Rahman on 12/2/17.
//  Copyright © 2017 Mostafizur Rahman. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

@protocol CMDialogInterface<NSObject>
//-(instancetype)initWithData:(NSMutableDictionary *)dialogData;
-(void)setMessageBody:(NSString *)msgBody;
-(void)setTitle:(NSString *)dialogTitle;
-(void)setImage:(UIImage *)image;
-(void)setRootViewController:(id)viewController;
-(void)removeAdd;
@end
