//
//  CMDialogBase.h
//  ColorMagic
//
//  Created by Mostafizur Rahman on 12/2/17.
//  Copyright © 2017 Mostafizur Rahman. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>
#import "CMDialogInterface.h"
#import "CMDialogView.h"

@interface CMDialogBase : UIView<CMDialogInterface>
-(instancetype)initWithDialogType:(unsigned int) d_type;

@end
