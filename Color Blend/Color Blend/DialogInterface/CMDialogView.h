//
//  CMPopUpView.h
//  ColorMagic
//
//  Created by Mostafizur Rahman on 12/2/17.
//  Copyright © 2017 Mostafizur Rahman. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "StartView.h"
@import GoogleMobileAds;

@protocol DialogButtonDelegate<NSObject>
-(void)clicked_Okay;
-(void)clicked_Cancel;
-(void)clicked_YES;
@end

@interface CMDialogView : StartView
#pragma mark- ADD_COMPONENTS
@property (weak, nonatomic) IBOutlet GADBannerView *add_bannerView;

@property (weak, nonatomic) IBOutlet UIView *contentParentView;
#pragma marks- DIALOG_COMPONENTS

@property (weak, nonatomic) IBOutlet UILabel *dialog_headerLabel;
@property (weak, nonatomic) IBOutlet UILabel *dialog_messageLabel;
@property (weak, nonatomic) IBOutlet UIImageView *dialog_imageView;
@property (weak, nonatomic) IBOutlet UIButton *dialog_yesButton;
@property (weak, nonatomic) IBOutlet UIButton *dialog_cancelButton;
@property (weak, nonatomic) IBOutlet UIButton *dialog_okayButton;
@property (weak, nonatomic) IBOutlet UIView *dialog_containerView;

@property (readwrite, weak) id<DialogButtonDelegate> dialogDelegate;

-(void)initialize_Ad:(id)rootViewController;
-(void)remove_Ad;
@end
