//
//  CMDialogBase.m
//  ColorMagic
//
//  Created by Mostafizur Rahman on 12/2/17.
//  Copyright © 2017 Mostafizur Rahman. All rights reserved.
//

#import "CMDialogBase.h"

@interface CMDialogBase(){
    
    int dialogType;
    
}
@end

@implementation CMDialogBase
static CMDialogBase *dialogBase = nil;
static CMDialogView *nib_DialogView;
-(instancetype)initWithDialogType:(unsigned int)type{
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        dialogBase = [super init];
        dialogBase.bounds = [[UIScreen mainScreen] bounds];
        nib_DialogView = [[NSBundle mainBundle] loadNibNamed:@"CMDialogView" owner:nil options:nil].firstObject;
        nib_DialogView.frame = dialogBase.bounds;
        nib_DialogView.dialog_okayButton.hidden = YES;
        [dialogBase addSubview:nib_DialogView];
        
    });
    
    dialogType = type;
    if(type == 0){
        nib_DialogView.dialog_okayButton.hidden = YES;
        nib_DialogView.dialog_cancelButton.hidden = NO;
        nib_DialogView.dialog_yesButton.hidden = NO;
    } else if(type == 1) {
        nib_DialogView.dialog_cancelButton.hidden = YES;
        nib_DialogView.dialog_yesButton.hidden = YES;
        nib_DialogView.dialog_okayButton.hidden = NO;
    }
    return dialogBase;
}

-(void)changeDialogType:(int )type{
    
}



-(void)setTitle:(NSString *)dialogTitle{
    nib_DialogView.dialog_headerLabel.text = dialogTitle;
}

-(void)setMessageBody:(NSString *)msgBody{
    nib_DialogView.dialog_messageLabel.text = msgBody;
}

-(void)setImage:(UIImage *)image{
    nib_DialogView.dialog_imageView.image = image;
}

-(void)setRootViewController:(id)viewController{
    [nib_DialogView initialize_Ad:viewController];
}

-(void)removeAdd{
    [nib_DialogView removeFromSuperview];
    [nib_DialogView remove_Ad];
}

@end

