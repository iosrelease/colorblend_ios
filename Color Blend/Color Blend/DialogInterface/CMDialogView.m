//
//  CMPopUpView.m
//  ColorMagic
//
//  Created by Mostafizur Rahman on 12/2/17.
//  Copyright © 2017 Mostafizur Rahman. All rights reserved.
//

#import "CMDialogView.h"
#import "CMAddCreator.h"
#import "BCInterfaceHelper.h"

@interface CMDialogView(){
    int type;
    CMAddCreator *addCreator;
}

@end

@implementation CMDialogView


-(void)drawRect:(CGRect)rectP{
    [super drawRect:rectP];
}

static CMDialogView *dialogView = nil;

-(instancetype)initWithCoder:(NSCoder *)aDecoder{
    
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        
        dialogView = [super initWithCoder:aDecoder];
    });
    return dialogView;
}

-(void)setLayers{
    
    dialogView.dialog_okayButton.layer.borderWidth = 0.56;
    dialogView.dialog_okayButton.layer.borderColor = [[UIColor whiteColor] CGColor];
    
    dialogView.dialog_cancelButton.layer.borderWidth = 0.56;
    dialogView.dialog_cancelButton.layer.borderColor = [[UIColor whiteColor] CGColor];
    
    dialogView.dialog_yesButton.layer.borderWidth = 0.56;
    dialogView.dialog_yesButton.layer.borderColor = [[UIColor whiteColor] CGColor];
    dialogView.dialog_containerView.layer.borderColor = [UIColor whiteColor].CGColor;
    dialogView.dialog_containerView.layer.borderWidth = 0.56;
}

-(void)initialize_Ad:(id)rootViewController{
    [dialogView setLayers];
    dialogView.dialogDelegate = rootViewController;
    dialogView.add_bannerView.rootViewController = rootViewController;
    addCreator = [[CMAddCreator alloc] initWithBanner:self.add_bannerView];
}

#pragma mark- DIALOG_ACTIONS
- (IBAction)dialog_Okay:(UIButton *)sender {
    type = 0;
    [dialogView remove_Ad];
}

- (IBAction)dialog_Yes:(UIButton *)sender {
    type = 2;
    [dialogView remove_Ad];
}
- (IBAction)bialog_Cancel:(UIButton *)sender {
    type = 1;
    [dialogView remove_Ad];
}

-(void)remove_Ad{
    [BCInterfaceHelper animateOpacityViewToInvisible:dialogView.superview anim_completion:^(BOOL finished) {
        if(type == 0)[dialogView.dialogDelegate clicked_Okay];
        else if(type == 1)[dialogView.dialogDelegate clicked_Cancel];
        else if(type == 2)[dialogView.dialogDelegate clicked_YES];
        dialogView.add_bannerView.rootViewController = nil;
        dialogView.dialogDelegate = nil;
        addCreator = nil;
    }];
}
@end
