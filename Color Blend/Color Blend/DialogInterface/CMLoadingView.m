//
//  CMLoadingView.m
//  ColorMagic
//
//  Created by Mostafizur Rahman on 11/22/17.
//  Copyright © 2017 Mostafizur Rahman. All rights reserved.
//

#import "CMLoadingView.h"
#import "BCDefinedConstant.h"
#import "CMAddCreator.h"

@interface CMLoadingView (){
    UIActivityIndicatorView *activity;
    CMAddCreator *add_creator;
}

@end

@implementation CMLoadingView

static CMLoadingView *loadingView;
-(instancetype)init{
    UIVisualEffect *effect = [UIBlurEffect effectWithStyle:UIBlurEffectStyleLight];
    
    self.layer.backgroundColor = [UIColor colorWithRed:.8 green:.4 blue:.7 alpha:1].CGColor;
    self = [super initWithEffect:effect];
    return self;
}

+(instancetype)sharedLoadingView{
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        loadingView = [[CMLoadingView alloc] init];
        [loadingView initializeSettings];
    });
    return loadingView;
}

-(BOOL)isAnimating{
    return activity.isAnimating;
}



-(void)setFrame:(CGRect)frame{
    [super setFrame:CGRectMake(frame.origin.x, frame.origin.y,
                               frame.size.width + 2,
                               frame.size.height + 2)];
    activity.center = loadingView.center;
    [activity startAnimating];
}

+(BOOL)isLoading{
    return [[CMLoadingView sharedLoadingView] isAnimating];
}

+(void)removeVisulaEffectFromView:(UIView *)parentView {
    [loadingView removeFromSuperview];
}

+(void)addVisualEffectOnView:(UIView *)parentView{
    if(loadingView == nil){
        loadingView = [CMLoadingView sharedLoadingView];
    }
    loadingView.frame = parentView.bounds;
    [parentView addSubview:loadingView];
}


-(void)initializeSettings{
    activity = [[UIActivityIndicatorView alloc]
                initWithFrame:CGRectMake(0, 0, 40*MAINSCRN_SCALE, 40*MAINSCRN_SCALE)];
    activity.activityIndicatorViewStyle = UIActivityIndicatorViewStyleWhiteLarge;
    activity.color = [UIColor colorWithRed:1 green:103/255 blue:114/255 alpha:1];
    activity.tag = MEDIATAG_ACTIVITY;
    [loadingView.contentView addSubview:activity];
}

-(void)removeFromSuperview{
    [super removeFromSuperview];
    [activity stopAnimating];
}

@end
