//
//  CMMediaProperties.h
//  ColorMagic
//
//  Created by Mostafizur Rahman on 11/23/17.
//  Copyright © 2017 Mostafizur Rahman. All rights reserved.
//
#import <UIKit/UIKit.h>
#import <ImageIO/ImageIO.h>
#import <AssetsLibrary/AssetsLibrary.h>

typedef enum {
    CMMediaTypeAlbum,
    CMMediaTypeThumbnail,
    CMMediaTypeSourceImage
}CMMediaType;
#import <Foundation/Foundation.h>

@interface CMMediaProperties : NSObject
@property (readwrite) NSString *mediaTitle;
@property (readwrite) CMMediaType mediaType;
@property (readwrite) ALAsset *mediaAsset;
@property (readwrite) long long assetCreationDate;

@property (readwrite) UIImage *iconImage;
@property (readwrite) NSURL *mediaUrl;

@end

@interface CMMediaGroup : NSObject

@property (readwrite) ALAssetsGroup *assetGroup;
@property (readwrite) BOOL isAssetGroupEdited;
@property (readwrite) long long assetCount;
@property (readwrite) NSString *groupName;
@property (readwrite) ALAssetsGroupType groupType;

@end
