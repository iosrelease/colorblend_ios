//
//  CMMediaProperties.m
//  ColorMagic
//
//  Created by Mostafizur Rahman on 11/23/17.
//  Copyright © 2017 Mostafizur Rahman. All rights reserved.
//

#import "CMMediaProperties.h"

@implementation CMMediaProperties
@synthesize mediaType;
@synthesize mediaTitle;
@synthesize assetCreationDate;
@end

@implementation CMMediaGroup

@synthesize groupName;
@synthesize groupType;
@synthesize isAssetGroupEdited;
@synthesize assetCount;
@synthesize assetGroup;

@end
