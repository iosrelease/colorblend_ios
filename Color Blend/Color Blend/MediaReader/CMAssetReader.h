//
//  CMAssetReader.h
//  ColorMagic
//
//  Created by Mostafizur Rahman on 11/23/17.
//  Copyright © 2017 Mostafizur Rahman. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "BCMediaContentViewController.h"

@protocol AssetReadingDelegate <NSObject>

-(void)onAssetReadingCompletion:(NSMutableArray *)assetArray;
-(void)didImageReadingCompleted:(UIImage *)originalImage;

@end

@interface CMAssetReader : NSObject

+(instancetype)sharedAssetReader;
-(void)startReadingGalleryImages:(__weak id<AssetReadingDelegate>)completionDelegate;
@end
