//
//  CMAssetReader.m
//  ColorMagic
//
//  Created by Mostafizur Rahman on 11/23/17.
//  Copyright © 2017 Mostafizur Rahman. All rights reserved.
//

#import "CMAssetReader.h"
#import <AssetsLibrary/AssetsLibrary.h>
#import "NSMutableArray+QueueStack.h"
#import "CMMediaProperties.h"


@interface CMAssetReader(){
    ALAssetsLibrary *assetLibrary;
    NSMutableArray *albumAssetArray;
    __weak id<AssetReadingDelegate> readCompleteDelegate;
    NSMutableArray *imageAssetArray;
}
@end

@implementation CMAssetReader

-(instancetype)init{
    self = [super init];
    assetLibrary = [[ALAssetsLibrary alloc] init];
    imageAssetArray = [[NSMutableArray alloc] init];
    return self;
}

static CMAssetReader *assetReader = nil;

//singlegon pattern is used to get all updated data from iPhone Photos app
//it requires minimum operations + time to get data after reading all the
//gallery images. once the static object is initialized, image loading is faster
//than previous one.
+(instancetype)sharedAssetReader{
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        assetReader = [[CMAssetReader alloc] init];
    });
    return assetReader;
}

-(void)readAlbumList{
    __weak CMAssetReader *weakMediaReader  = self;
    if([albumAssetArray count] > 0){
        [albumAssetArray removeAllObjects];
    }
    albumAssetArray  = [[NSMutableArray alloc] init];
    [assetLibrary enumerateGroupsWithTypes:ALAssetsGroupAll usingBlock:
     ^(ALAssetsGroup *albumGroup, BOOL *stop) {
        const int assetCounts = (int)[albumGroup numberOfAssets];
        if(assetCounts > 0 && albumGroup != nil) {
            CMMediaGroup *mediaGroup = [[CMMediaGroup alloc] init];
            mediaGroup.assetGroup = albumGroup;
            mediaGroup.groupName = [albumGroup valueForProperty:ALAssetsGroupPropertyName];
            mediaGroup.groupType = (ALAssetsGroupType)
            [[albumGroup valueForProperty:ALAssetsGroupPropertyType] intValue];
            mediaGroup.assetCount = assetCounts;
            mediaGroup.isAssetGroupEdited = assetCounts != mediaGroup.assetCount;
            [albumAssetArray addObject:mediaGroup];
        } else {
            [weakMediaReader setGalleryImages:albumAssetArray shouldUpdate:imageAssetArray.count != 0];
        }
        
    } failureBlock: ^(NSError *error) {
        
    }];
}

-(BOOL)newAlbumFound:(ALAssetsGroup *)albumGroup{
    NSURL *albumUrl = [albumGroup valueForProperty: ALAssetsGroupPropertyURL];
    for(CMMediaGroup *mediaGroup in albumAssetArray){
        if([[[mediaGroup.assetGroup valueForProperty:ALAssetsGroupPropertyURL] path]
            isEqualToString:[albumUrl path]]){
            return NO;
        }
    }
    return YES;
}

-(void)startReadingGalleryImages:(id<AssetReadingDelegate>)completionDelegate{
    if(readCompleteDelegate){
        readCompleteDelegate = nil;
    }
    readCompleteDelegate = completionDelegate;
    [self readAlbumList];
}

-(void)setGalleryImages:(NSMutableArray *)albumArray
           shouldUpdate:(const BOOL)shouldUpdate{
    __weak CMAssetReader *weakMediaReader  = self;
    __block int album_count = (int)[albumArray count];
    for (CMMediaGroup *mediaGroup in albumArray) {
        album_count--;
        [mediaGroup.assetGroup enumerateAssetsWithOptions:NSEnumerationReverse usingBlock:
         ^(ALAsset *asset, NSUInteger index, BOOL *innerStop) {
             if (asset) {
                 NSDate *creationNSDate = [asset valueForProperty:ALAssetPropertyDate];
                 const long long creationDate = [creationNSDate timeIntervalSince1970];
                 const int insertIndex = [[self class] getSearchIndex:imageAssetArray imageCreationDate:creationDate];
                 BOOL shouldInsert = [self shouldInsertObject:asset atIndex:insertIndex];
                 if(shouldInsert){
                     [imageAssetArray insertObject:asset atIndex:insertIndex];
                 } else {
                     if(shouldUpdate){
                         *innerStop = YES;
                         album_count = 0;
                         index = 0;
                     }
                 }
                 if(album_count == 0 && index == 0) {
                     [weakMediaReader didMediaReadCompleted];
                 }
             }
         }];
    }
}

-(BOOL)shouldInsertObject:(ALAsset *)imageAsset atIndex:(const int)insertedIndex{
    if (insertedIndex < [imageAssetArray count]) {
        NSDate *creationDate = [imageAsset valueForProperty:ALAssetPropertyDate];
        const long long long_cdate = [creationDate timeIntervalSince1970];
        NSString *absPathString = [[imageAsset valueForProperty:ALAssetPropertyAssetURL] absoluteString];
        for(int index = insertedIndex; index< [imageAssetArray count]; index++){
            ALAsset *nextAsset = [imageAssetArray objectAtIndex:index];
            creationDate = [nextAsset valueForProperty:ALAssetPropertyDate];
            const long long long_idate = [creationDate timeIntervalSince1970];
            if(long_cdate != long_idate){
                break;
            }
            NSString *pathString = [[nextAsset valueForProperty:ALAssetPropertyAssetURL] absoluteString];
            if([absPathString isEqualToString:pathString]){
                return NO;
            }
        }
        return YES;
    }
    
    return YES;
}




-(void)didMediaReadCompleted{
    if(readCompleteDelegate != nil){
        [readCompleteDelegate onAssetReadingCompletion:imageAssetArray];
    }
}

+(int)getSearchIndex:(NSMutableArray *)arrayTobeSorted
   imageCreationDate:(const long long) creationDate {
    int startIndex = 0;
    int endIndex = (int)arrayTobeSorted.count - 1;
    int middleIndex = (startIndex + endIndex) / 2;
    while (startIndex <= endIndex) {
        ALAsset *imageAsset = [arrayTobeSorted objectAtIndex:middleIndex];
        NSDate *creationNSDate = [imageAsset valueForProperty:ALAssetPropertyDate];
        const long long c_date = [creationNSDate timeIntervalSince1970];
        if(c_date < creationDate) {
            startIndex = middleIndex + 1;
        } else if (c_date == creationDate) {
            return middleIndex;
        } else {
            endIndex = middleIndex - 1;
        }
        middleIndex = (startIndex + endIndex)/2;
    }
    return startIndex;
}

-(void)getFullResolutionImage:(NSURL *)assetUrl {
    [assetLibrary assetForURL:assetUrl resultBlock:^(ALAsset *asset) {
        if (asset) {
            ALAssetRepresentation *representation = [asset defaultRepresentation];
            UIImage *originalImage = [UIImage imageWithCGImage:[representation fullResolutionImage]
                                                         scale:[representation scale] orientation:(UIImageOrientation)representation.orientation];
            [readCompleteDelegate didImageReadingCompleted:originalImage];
        }
    }failureBlock:^(NSError *error) { }];
}

-(void)getFullScreenImage:(NSURL *)assetUrl onViewController:(BCMediaContentViewController *__weak)pageContentViewController dFormatter:(NSDateFormatter *)dateFormatter{
    [assetLibrary assetForURL:assetUrl resultBlock:^(ALAsset *asset) {
        if (asset) {
            ALAssetRepresentation *representation = [asset defaultRepresentation];
            UIImage *image = [UIImage imageWithCGImage:[representation fullScreenImage]
                                             scale:[representation scale] orientation:UIImageOrientationUp];
            dispatch_async(dispatch_get_main_queue(), ^{
                [pageContentViewController setOrginalImage:image];
            });
        }
    }failureBlock:^(NSError *error) { }];
}

@end
