//
//  MonochromeView.m
//  Color Blend
//
//  Created by Mostafizur Rahman on 12/27/17.
//  Copyright © 2017 liilab. All rights reserved.
//

#import "MonochromeView.h"
#import "BCInterfaceHelper.h"


@interface MonochromeView(){
    EAGLContext *eagl_context;
    CIContext *drawing_context;
    CIFilter *magicFilter;
    CIImage *sourceCoreImage;
    
    CGRect sourceImageExtent;
    CGRect destinationExtent;
    CIColor *__inputCoreColor;
    NSString *__inputColorKey;
    NSString *__inputSliderKey;
    CGFloat __curSliderValue;
    BOOL shouldInitializeContext;
}

@property (readwrite) CGFloat maxSliderValue;
@property (readwrite) CGFloat minSliderValue;


@end

@implementation MonochromeView


-(instancetype)initWithCoder:(NSCoder *)aDecoder{
    self = [super initWithCoder:aDecoder];
    shouldInitializeContext = YES;
    return self;
}

// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
    [super drawRect:rect];
    [drawing_context drawImage:magicFilter.outputImage inRect:destinationExtent fromRect:sourceImageExtent];
    [eagl_context presentRenderbuffer:GL_RENDERBUFFER];
}

-(void)dealloc{
    if (@available(iOS 10.0, *)) {
        [drawing_context clearCaches];
    } else {
        // Fallback on earlier versions
    }
    [magicFilter setValue:nil forKey:@"inputImage"];
    [EAGLContext setCurrentContext:nil];
    eagl_context = nil;
    drawing_context = nil;
    magicFilter = nil;
//    [self deleteDrawable];
}

-(BOOL)setupDrawingContext{
    if(shouldInitializeContext){
        shouldInitializeContext = NO;
        self.clipsToBounds = YES;
        
        eagl_context = [[EAGLContext alloc] initWithAPI:kEAGLRenderingAPIOpenGLES2];
        drawing_context = [CIContext contextWithEAGLContext:eagl_context];
        self.context = eagl_context;
        CAEAGLLayer* _eaglLayer = (CAEAGLLayer*) self.layer;
        _eaglLayer.opaque = NO;
        _eaglLayer.drawableProperties = [NSDictionary dictionaryWithObjectsAndKeys: kEAGLColorFormatRGBA8,
                                         kEAGLDrawablePropertyColorFormat, nil];
//        [self bindDrawable];
        
        glClearColor(1.50, 0.50, 0.50, 1.0);
        glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
        glEnable(GL_BLEND);
        glBlendFunc(GL_ONE, GL_ONE_MINUS_SRC_ALPHA);
        if (eagl_context != [EAGLContext currentContext])
            [EAGLContext setCurrentContext:eagl_context];
        self.enableSetNeedsDisplay = YES;
    }
    return !shouldInitializeContext;
}

+(Class)class{
    return CAEAGLLayer.class;
}

-(void)setupSourceImage:(UIImage *)sourceImage{
    sourceCoreImage = [[CIImage alloc] initWithImage:sourceImage];
    sourceImageExtent = sourceCoreImage.extent;
    
    destinationExtent = [BCInterfaceHelper getViewRect:sourceImage.size parentViewSize:MAINSCRN_SIZE withPaddingOffset:0];
    const CGFloat width = destinationExtent.size.width * self.contentScaleFactor;
    const CGFloat height = destinationExtent.size.height * self.contentScaleFactor;
    
    const CGFloat maxWidth = MAINSCRN_WIDTH * self.contentScaleFactor;
    const CGFloat maxHeight = MAINSCRN_HEIGHT * self.contentScaleFactor;
    
    const CGFloat originX = maxWidth / 2 - width / 2;
    const CGFloat originY = maxHeight / 2 - height / 2;
    destinationExtent = CGRectMake(originX - 1, originY - 1, width + 2, height + 2);
    [self initializeChromeFilter];
    [self setNeedsDisplay];
}

-(void)initializeChromeFilter{
    magicFilter = [CIFilter filterWithName:@"CIColorMonochrome"];
    [magicFilter setValue:sourceCoreImage forKey:kCIInputImageKey];
    [magicFilter setDefaults];
    
    NSArray *inputKeys = magicFilter.inputKeys;
    NSDictionary *attributes = magicFilter.attributes;
    for(NSString *key in inputKeys){
        if([key isEqualToString:@"inputImage"]){
            continue;
        }
        
        NSDictionary *attribute = [attributes objectForKey:key];
        NSArray *allKeys = [attribute allKeys];
        if([key containsString:@"Color"]){
            __inputColorKey = key;
            __inputCoreColor = [attribute objectForKey:kCIAttributeDefault];
        } else {
            __inputSliderKey = key;
            _minSliderValue = [allKeys containsObject:kCIAttributeSliderMin] ?
            [[attribute objectForKey:kCIAttributeSliderMin] floatValue] : 100000;
            _maxSliderValue = [allKeys containsObject:kCIAttributeSliderMax] ?
            [[attribute objectForKey:kCIAttributeSliderMax] floatValue] : -100000;
            __curSliderValue = [allKeys containsObject:kCIAttributeDefault] ?
            [[attribute objectForKey:kCIAttributeDefault] floatValue] : 0;
            [magicFilter setValue:[NSNumber numberWithFloat:0.25] forKey:__inputSliderKey];
        }
    }
}

-(UIImage *)getOutputImage{
    CGImageRef imageRef = [drawing_context createCGImage:magicFilter.outputImage fromRect:sourceImageExtent];
    UIImage *outputImage = [UIImage imageWithCGImage:imageRef];
    CGImageRelease(imageRef);
    return outputImage;
}

-(void)setFilterColor:(UIColor *)input_color{
    dispatch_async(dispatch_get_main_queue(), ^{
        __inputCoreColor = [CIColor colorWithCGColor:input_color.CGColor];
        [magicFilter setValue:__inputCoreColor forKey:__inputColorKey];
        [self setNeedsDisplay];
    });
}

-(void)changeIntensityValue:(const CGFloat)slider_value{
    dispatch_async(dispatch_get_main_queue(), ^{
        __curSliderValue = slider_value;
        [magicFilter setValue:[NSNumber numberWithFloat:slider_value] forKey:__inputSliderKey];
        [self setNeedsDisplay];
    });
}



@end
