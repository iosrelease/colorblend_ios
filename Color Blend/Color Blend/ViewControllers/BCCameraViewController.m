//
//  BCCameraViewController.m
//  BucketCam
//
//  Created by Mostafizur Rahman on 3/29/17.
//  Copyright © 2017 Mostafizur Rahman. All rights reserved.
//


#import "BCCameraViewController.h"
#import "BCInterfaceHelper.h"
#import "BlendViewController.h"

#import <AssetsLibrary/AssetsLibrary.h>

@interface BCCameraViewController (){
    
    BCCameraSessionController *sessionController;
    BOOL shouldSkipTouch;
}

@end

@implementation BCCameraViewController

-(void)dealloc{
    sessionController.imageCaptureDelegate  = nil;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    //circled capture button.
    [BCInterfaceHelper setButtonLayer:self.buttonCollections];
    //initialize session controller for camera handling
    if(!sessionController){
        sessionController = [BCCameraSessionController sharedCameraSession];
        sessionController.imageCaptureDelegate = self;
    }
    //flash mode is off by default
    [sessionController setFlashModeOff];
}

-(void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    dispatch_async(dispatch_get_main_queue(), ^{
        
        [sessionController shouldStartSession];
        shouldSkipTouch = NO;
        //set up camera preview layer bounds
        sessionController.previewLayer.frame = CGRectMake(0, 0, MAINSCRN_WIDTH, MAINSCRN_HEIGHT);
        [self.view.layer insertSublayer:sessionController.previewLayer atIndex:0];
        
        
    });
    
}

-(void)viewDidAppear:(BOOL)animated{
    [super viewDidAppear:animated];
}

-(void)viewDidDisappear:(BOOL)animated{
    [super viewDidDisappear:animated];
    //remove preview layer upon exit
    [sessionController.previewLayer removeFromSuperlayer];
}

-(void)viewWillDisappear:(BOOL)animated{
    [super viewWillDisappear:animated];
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)capturePhoto:(id)sender {
    //single image captured during multiple capture requests
    if([sessionController.cameraCaptureSession  isRunning] && !shouldSkipTouch){
        shouldSkipTouch = YES;
        [sessionController captureStillImage];
    }
}

- (IBAction)exitCameraViewController:(id)sender {
    [sessionController shouldStopSession];
    [self.navigationController popViewControllerAnimated:YES];
}

- (IBAction)flipCamera:(UIButton *)sender {
    [sessionController changeCamera];
    
}

- (IBAction)changeFlush:(UIButton *)sender {
    //change flash button icon for flash mode changes.
    if([sessionController flashMode]){
        [sender setImage:[UIImage imageNamed:@"flash"] forState:UIControlStateNormal];
    } else {        
        [sender setImage:[UIImage imageNamed:@"flash_no"] forState:UIControlStateNormal];
    }
}


#pragma mark - Delegation

-(void)onImageCaptured:(UIImage *)displayImage{
    //programatically create color pop view controller to support capturing delays.
    //if flash is on, delay increases.
    
    dispatch_async(dispatch_get_main_queue(), ^{
        BlendViewController *colorPopViewController = [self.storyboard instantiateViewControllerWithIdentifier:@"BlendViewController"];
        colorPopViewController.sourceImage = displayImage;
        
        [self.navigationController pushViewController:colorPopViewController animated:YES];
    });
}



@end
