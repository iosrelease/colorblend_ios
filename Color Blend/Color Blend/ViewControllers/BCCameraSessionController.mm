//
//  BCCameraSessionController.m
//  BucketCam
//
//  Created by Mostafizur Rahman on 3/29/17.
//  Copyright © 2017 Mostafizur Rahman. All rights reserved.
//
#import <ImageIO/ImageIO.h>


#import "BCCameraSessionController.h"
#import "AppDelegate.h"
#import "BCDefinedConstant.h"

#define MAX_IOSVERSION @"9.0"
#define QUEUE_VIDEO "com.colorpop.video_queue"

@interface BCCameraSessionController() {
    
    dispatch_queue_t videoBufferQueue;
    AVCaptureConnection *videoConnection;
    CGRect videoPreviewViewBounds;
    AVCaptureVideoDataOutput *videoDataOutput;
    AVCaptureStillImageOutput *stillImageOutput;
    AVCaptureConnection *stillImageConnection;
    BOOL shouldStopRendering;
    BOOL isFrontFacingCamera;
}

@property (readwrite) AVCaptureSession *cameraCaptureSession;
@property (readwrite) AVCaptureVideoPreviewLayer *previewLayer;
@property (readwrite) BOOL hasBackCameraSupport;
@property (readwrite) BOOL isFlashModeON;
@end

@implementation BCCameraSessionController
@synthesize cameraCaptureSession;
@synthesize previewLayer;
@synthesize hasBackCameraSupport;
@synthesize isFlashModeON;

static dispatch_once_t dispatch_1;
static BCCameraSessionController *sessionController = nil;
-(void)deleteSharedResources{
    [cameraCaptureSession stopRunning];
    NSArray *inputComponentArray = [cameraCaptureSession inputs];
    [cameraCaptureSession beginConfiguration];
    for(id object in inputComponentArray){
        [cameraCaptureSession removeInput:object];
    }
    [cameraCaptureSession commitConfiguration];
    cameraCaptureSession = nil;
    videoDataOutput = nil;
    dispatch_1 = 0;
    videoConnection = nil;
    stillImageConnection = nil;
    stillImageOutput = nil;
    videoDataOutput = nil;
    previewLayer = nil;
    self.imageCaptureDelegate = nil;
}

+(instancetype)sharedCameraSession{
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        sessionController = [[BCCameraSessionController alloc] init];
        [sessionController startCameraSession];
    });
    return sessionController;
}

-(void)startCameraSession {
    cameraCaptureSession = [[AVCaptureSession alloc] init];
    [cameraCaptureSession setSessionPreset:AVCaptureSessionPresetHigh];
    [self setCameraInSession:cameraCaptureSession];
    [self setVideoDataOutptFromSession:cameraCaptureSession];
    [self setVideoPreviewLayer:cameraCaptureSession];
    
}

-(BOOL)changeCamera{
    if(hasBackCameraSupport){
        AVCaptureDevicePosition desiredPosition;
        if (isFrontFacingCamera)
            desiredPosition = AVCaptureDevicePositionBack;
        else
            desiredPosition = AVCaptureDevicePositionFront;
        
        for (AVCaptureDevice *d in [AVCaptureDevice devicesWithMediaType:AVMediaTypeVideo]) {
            if ([d position] == desiredPosition) {
                [[previewLayer session] beginConfiguration];
                AVCaptureDeviceInput *input = [AVCaptureDeviceInput deviceInputWithDevice:d error:nil];
                for (AVCaptureInput *oldInput in [[previewLayer session] inputs]) {
                    [[previewLayer session] removeInput:oldInput];
                }
                [[previewLayer session] addInput:input];
                stillImageConnection = [stillImageOutput connectionWithMediaType:AVMediaTypeVideo];
                [[previewLayer session] commitConfiguration];
                break;
            }
        }
        isFrontFacingCamera = !isFrontFacingCamera;
    }
    return isFrontFacingCamera;
}


-(void)setVideoPreviewLayer:(AVCaptureSession *)session{
    previewLayer = [[AVCaptureVideoPreviewLayer alloc] initWithSession:session];
    previewLayer.videoGravity = AVLayerVideoGravityResizeAspect;
}

-(BOOL)flashMode{
    isFlashModeON = NO;
    NSArray *devices = AVCaptureDevice.devices;
    for(AVCaptureDevice *device in devices) {
        if((device.position == AVCaptureDevicePositionBack
           || device.position == AVCaptureDevicePositionFront) && [device isFlashModeSupported:AVCaptureFlashModeOn]) {
            [device lockForConfiguration:nil];
            isFlashModeON = device.flashMode != AVCaptureFlashModeOn;
            device.flashMode = isFlashModeON ? AVCaptureFlashModeOn : AVCaptureFlashModeOff;
            [device unlockForConfiguration];
        }
    }
    return isFlashModeON;
}

-(void)setFlashModeOff{
    if(isFlashModeON){
        [self flashMode];
    }
}

-(void)setCameraInSession:(AVCaptureSession *)session {
    
    AVCaptureDevice *videoDevice = [AVCaptureDevice defaultDeviceWithMediaType:AVMediaTypeVideo];
    
    NSArray *devices = AVCaptureDevice.devices;
    hasBackCameraSupport = [devices count] > 1;
    for(AVCaptureDevice *device in devices) {
        if(device.position == AVCaptureDevicePositionBack) {
            videoDevice = device;
            break;
        }
    }
    AVCaptureDeviceInput *capInput = [AVCaptureDeviceInput deviceInputWithDevice:videoDevice error:nil];
    if (capInput)
        [session addInput:capInput];
    stillImageOutput = [[AVCaptureStillImageOutput alloc] init];
    NSDictionary *outputSettings = @{ AVVideoCodecKey : AVVideoCodecJPEG};
    [stillImageOutput setOutputSettings:outputSettings];
    stillImageConnection = nil;
    if([session canAddOutput:stillImageOutput] ) {
        [session addOutput:stillImageOutput];
    }
    stillImageConnection = [stillImageOutput connectionWithMediaType:AVMediaTypeVideo];
}

-(void)setVideoDataOutptFromSession:(AVCaptureSession *)session
{
    videoBufferQueue = dispatch_queue_create(QUEUE_VIDEO, DISPATCH_QUEUE_SERIAL);
    videoDataOutput =  [[AVCaptureVideoDataOutput alloc] init];
//    [videoDataOutput setSampleBufferDelegate:self queue:videoBufferQueue];
    
    NSDictionary *videoDataSettings = [NSDictionary dictionaryWithObject:
                                       [NSNumber numberWithInt:kCVPixelFormatType_32BGRA]
                                                                  forKey:(id)kCVPixelBufferPixelFormatTypeKey];
    videoDataOutput.videoSettings = videoDataSettings;
    if([session canAddOutput:videoDataOutput]) {
        [session addOutput:videoDataOutput];
    }
    videoConnection = [videoDataOutput connectionWithMediaType:AVMediaTypeVideo];
    if (videoConnection) {
        videoConnection.videoOrientation = AVCaptureVideoOrientationPortrait;
    }
    
}

-(void)captureOutput:(AVCaptureOutput *)captureOutput
didOutputSampleBuffer:(CMSampleBufferRef)sampleBuffer
      fromConnection:(AVCaptureConnection *)connection{
    
//    if ([captureOutput isKindOfClass:[AVCaptureVideoDataOutput class]] && !shouldStopRendering) {
//        CVImageBufferRef imageBuffer = CMSampleBufferGetImageBuffer(sampleBuffer);
//        
//       } else {
//           
//    }
    
}

-(void)captureStillImage{
    shouldStopRendering = YES;
    [stillImageOutput captureStillImageAsynchronouslyFromConnection:stillImageConnection completionHandler:
     ^(CMSampleBufferRef imageSampleBuffer, NSError *error) {
        if(shouldStopRendering){
            shouldStopRendering = NO;
            [self.cameraCaptureSession stopRunning];
            if(imageSampleBuffer != NULL){
                
                NSData *imageRawData = [AVCaptureStillImageOutput jpegStillImageNSDataRepresentation:imageSampleBuffer];
                dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_HIGH, 0), ^{
                    UIImage *rawImage = [[UIImage alloc] initWithData:imageRawData];
                    UIImage *capturedImage = [CMImageExtension getRotatedImage:rawImage];
                    [self.imageCaptureDelegate onImageCaptured:capturedImage ];
                });
            }
        }
     }];
}


-(void)shouldStopSession {
    if([self.cameraCaptureSession isRunning])
        [self.cameraCaptureSession stopRunning];
}

-(void)shouldStartSession{
    shouldStopRendering = NO;
    if(![self.cameraCaptureSession isRunning])
        [self.cameraCaptureSession  startRunning];
}
@end
