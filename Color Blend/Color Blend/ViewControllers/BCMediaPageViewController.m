//  BCMediaPageViewController.h
//  RingStudio
//
//  Created by Mostafizur Rahman on 5/21/15.
//  Copyright (c) 2015 BucketCam. All rights reserved.
//

#import "BCMediaPageViewController.h"
#import "BCDefinedConstant.h"
#import "BlendViewController.h"


@interface BCMediaPageViewController() {
    int currentIndex;
    
    BOOL animating;
    UIView *loadingView;
    NSInteger hvLayoutConstant;
    UIImage *displayImage;
    BOOL isFirstLaunch;
    NSDateFormatter *dateFormatter;
    long imageAssetCount;
}
@end


@implementation BCMediaPageViewController
@synthesize imageAssetArray;
@synthesize arrayIndex;


#pragma -mark System View Controller Delegate Methods

- (void)viewDidLoad
{
    [super viewDidLoad];
    dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"EEE, MMMM dd, yyyy"];
    currentIndex = arrayIndex;
    imageAssetCount = [imageAssetArray count] - 1;
    isFirstLaunch = YES;
    self.navigationController.navigationBar.hidden = NO;
    [self createPageViewController];
    self.pageTitleLabel.text = [NSString stringWithFormat:@"%d/%d",(currentIndex + 1), (int)[imageAssetArray count]];
    self.dateTakenLabel.text = [self getDateString];
}

-(void)viewWillDisappear:(BOOL)animated{
    [super viewWillDisappear:animated];
    NSArray *gestures = self.view.gestureRecognizers;
    for(UIGestureRecognizer *gesture in gestures){
        [gesture removeTarget:self action:@selector(recognizeTapGesture:)];
        [self.view removeGestureRecognizer:gesture];
    }
}

-(NSString *)getDateString{
    NSDate *creationDate = [[imageAssetArray objectAtIndex:imageAssetCount - currentIndex] valueForProperty: ALAssetPropertyDate];
    return [dateFormatter stringFromDate:creationDate];
}

-(void)createPageViewController{
    self.pageViewController = [self.storyboard instantiateViewControllerWithIdentifier:IDENTIFIER_MEDIAPAGE];
    self.pageViewController.delegate = self;
    self.pageViewController.dataSource = self;
    BCMediaContentViewController *startingViewController = [self viewController];
    if(!startingViewController) return;
    NSArray *viewControllers = @[startingViewController];
    [self.pageViewController setViewControllers:viewControllers
                                      direction:UIPageViewControllerNavigationDirectionForward
                                       animated:NO completion:nil];
    [self addChildViewController:self.pageViewController];
    [self.view addSubview:self.pageViewController.view];
    [self.pageViewController didMoveToParentViewController:self];
}

-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    [self.view bringSubviewToFront:self.headerView ];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

-(void)viewDidDisappear:(BOOL)animated {
    [super viewDidDisappear:animated];
    [[[self childViewControllers] firstObject] dismissViewControllerAnimated:YES completion:nil];
}

#pragma mark -Page View Controller Data Source

-(void)pageViewController:(UIPageViewController *)pageController
       didFinishAnimating:(BOOL)finished
  previousViewControllers:(NSArray *)previousViewControllers
      transitionCompleted:(BOOL)completed {
    
    BCMediaContentViewController *contentViewController = [pageController.viewControllers lastObject];
    currentIndex = (int)contentViewController.currentIndex;
    self.pageTitleLabel.text = [NSString stringWithFormat:@"%d/%ld", (currentIndex + 1), (unsigned long)[imageAssetArray count]];
    self.dateTakenLabel.text = [self getDateString];
    displayImage = contentViewController.connentImageView.image;
}

- (BCMediaContentViewController *)viewController {
    if (([imageAssetArray count] == 0) || (currentIndex >= [imageAssetArray count])) return nil;
    BCMediaContentViewController *pageContentViewController = (BCMediaContentViewController *) [self.storyboard                                                                                                    instantiateViewControllerWithIdentifier:IDENTIFIER_MEDIACONT];
    
    ALAsset *imageAsset = [imageAssetArray objectAtIndex:imageAssetCount - currentIndex];
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_HIGH, 0), ^{
        ALAssetRepresentation *representation = [imageAsset defaultRepresentation];
        UIImage *sourceImage = [UIImage imageWithCGImage:representation.fullScreenImage];
        if(displayImage == nil){
            displayImage = sourceImage;
        }
        dispatch_async(dispatch_get_main_queue(), ^{
            [pageContentViewController setOrginalImage:sourceImage];
        });
    });
    
    
    pageContentViewController.currentIndex = currentIndex;
    
    return pageContentViewController;
}

- (UIViewController *)pageViewController:(UIPageViewController *)pageViewController
      viewControllerBeforeViewController:(UIViewController *)viewController {
    if(currentIndex == 0){
        return nil;
    }
    currentIndex--;
    return [self viewController];
}

- (UIViewController *)pageViewController:(UIPageViewController *)pageViewController
       viewControllerAfterViewController:(UIViewController *)viewController {
    if(currentIndex  == [imageAssetArray count]){
        return nil;
    }
    currentIndex++;
    return [self viewController];
}

#pragma -mark Button Actions


- (IBAction)exitAlbumViewController:(id)sender {
    
    NSArray *viewControllers = [self childViewControllers];
    for(BCMediaContentViewController *viewController in viewControllers){
        [viewController dismissViewControllerAnimated:NO completion:nil];
    }
    [self.navigationController popViewControllerAnimated:YES];
}

-(void)recognizeTapGesture:(UITapGestureRecognizer *)tapGesture {
    if(animating)return;
    animating = YES;
    if(self.headerView.hidden){
        self.headerView.hidden = NO;
        
        [UIView animateWithDuration:ANIMATION_DURATION animations:^{
            self.headerView.center = CGPointMake(self.headerView.center.x, self.headerView.center.y + self.headerView.frame.size.height);
        } completion:^(BOOL finished) {
            animating  =  NO;
            self.hvTopLayoutConstraint.constant = hvLayoutConstant;
            [self.headerView setNeedsDisplay];
        }];
    } else {
        hvLayoutConstant = self.hvTopLayoutConstraint.constant;
        [UIView animateWithDuration:ANIMATION_DURATION animations:^{
            self.headerView.center = CGPointMake(self.headerView.center.x, self.headerView.center.y - self.headerView.frame.size.height);
        } completion:^(BOOL finished) {
            self.headerView.hidden = YES;
            self.hvTopLayoutConstraint.constant = -self.headerView.frame.size.height;
            [self.headerView setNeedsDisplay];
            animating = NO;
        }];
    }
}
- (IBAction)openEditorViewController:(id)sender {
    dispatch_async(dispatch_get_main_queue(), ^{
        BlendViewController *colorPopViewController = [self.storyboard instantiateViewControllerWithIdentifier:@"BlendViewController"];
        colorPopViewController.sourceImage = displayImage;

        [self.navigationController pushViewController:colorPopViewController animated:YES];
    });
}

- (IBAction)openShareInterfaces:(id)sender {
//    BCShareViewController *shareViewController = [self.storyboard instantiateViewControllerWithIdentifier:@"ShareVC"];
//    shareViewController.image = displayImage;
//    [self.navigationController pushViewController:shareViewController animated:YES];
}

//-(void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender{
//    if ([[segue identifier] isEqualToString:@"ColoPop_Segue"]) {
//        
//        CMColorViewController *colorPopViewController = [segue destinationViewController];
//        [colorPopViewController setSourceImage:displayImage];
//    }
//}

@end
