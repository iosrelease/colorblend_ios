//
//  BCCameraSesssionController.h
//  BucketCam
//
//  Created by Mostafizur Rahman on 3/29/17.
//  Copyright © 2017 Mostafizur Rahman. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <AVFoundation/AVFoundation.h>
#import "CMImageExtension.h"

@protocol CPImageCaptureDelegate<NSObject>
-(void)onImageCaptured:(UIImage *)image;
@end

@interface BCCameraSessionController : NSObject<AVCaptureVideoDataOutputSampleBufferDelegate, AVCaptureAudioDataOutputSampleBufferDelegate>
@property (readonly) AVCaptureSession *cameraCaptureSession;
@property (readonly) AVCaptureVideoPreviewLayer *previewLayer;
@property (readonly) BOOL hasBackCameraSupport;
@property (readonly) BOOL isFlashModeON;


+(instancetype)sharedCameraSession;
-(BOOL)changeCamera;
-(void)captureStillImage;
-(void)shouldStopSession;
-(void)shouldStartSession;
-(void)deleteSharedResources;
-(BOOL)flashMode;
-(void)setFlashModeOff;

@property (readwrite, weak) id<CPImageCaptureDelegate> imageCaptureDelegate;

@end
