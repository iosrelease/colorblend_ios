//
//  BCMediaCollectionViewController.m
//  BucketCam
//
//  Created by Mostafizur Rahman on 5/19/17.
//  Copyright © 2017 Mostafizur Rahman. All rights reserved.
//

#import "BCMediaCollectionViewController.h"
#import "BCMediaPageViewController.h"
#import "CMLoadingView.h"

#define SPACE_BC 4
#define NOOF_IPHONECELL 4
#define NOOF_IPADCELL 6


@interface BCMediaCollectionViewController(){
    NSMutableArray *imageAssetArray;
    int selectedImageIndex;
    CGFloat cv_cellSpacing;
    CGFloat cv_widthHeight;
    CGRect cellRect;
    CMAddCreator *add_creator;
}
@end


@implementation BCMediaCollectionViewController


-(void)viewDidLoad{
    [super viewDidLoad];
    [CMLoadingView sharedLoadingView];
//    mediaReader = [[BCMediaReader alloc] init];
//    mediaReader.mediaReaderDelegate = self;
//    [mediaReader getMediaStoreAlbums];
    [CMLoadingView addVisualEffectOnView:self.view];
    [self generateAdd];
    asset_reader = [CMAssetReader sharedAssetReader];
    [asset_reader startReadingGalleryImages:self];
}

-(void)onAssetReadingCompletion:(NSMutableArray *)image_assetArray{
    imageAssetArray = image_assetArray;
    [self.albumCollectionView reloadData];
    [CMLoadingView removeVisulaEffectFromView:self.view];
}

-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    [self initializeCellProperties:MAINSCRN_WIDTH];
}

-(void)initializeCellProperties:(const CGFloat) widht {
    cv_widthHeight = UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad ?
    widht / NOOF_IPADCELL : widht / NOOF_IPHONECELL;
    cv_widthHeight -= SPACE_BC ;
}

-(void)viewWillDisappear:(BOOL)animated{
    [super viewWillDisappear:animated];
//    mediaReader.mediaReaderDelegate = nil;
}


#pragma -mark UICOLLECTIONVIEW_FLOWDELEGATE

- (UIEdgeInsets)collectionView:(UICollectionView*)collectionView
                        layout:(UICollectionViewLayout *)collectionViewLayout
        insetForSectionAtIndex:(NSInteger)section {
    return UIEdgeInsetsMake(0, SPACE_BC/2, 0, SPACE_BC / 2);
}

- (CGFloat)collectionView:(UICollectionView *) collectionView
                   layout:(UICollectionViewLayout *)collectionViewLayout
minimumInteritemSpacingForSectionAtIndex:(NSInteger) section {
    return SPACE_BC / 2;
}

- (CGSize)collectionView:(UICollectionView *)collectionView
                  layout:(UICollectionViewLayout *)collectionViewLayout
  sizeForItemAtIndexPath:(NSIndexPath *)indexPath {
    return CGSizeMake(cv_widthHeight, cv_widthHeight);
}

#pragma -mark UICOLLECTIONVIEW_DATASOURCE

//-(NSMutableArray *)getMediaArray:(const NSInteger )section{
//    return [groupedMediaDictionary objectForKey:[grpMediaKeyArray objectAtIndex:section]];
//}

-(NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView{
    return 1;
}

-(NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section{
    return [imageAssetArray count];
}

-(UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath{
    UICollectionViewCell *mediaCell = [collectionView dequeueReusableCellWithReuseIdentifier:IDENTIFIER_MEDIACELL forIndexPath:indexPath];
    UIImageView *imageView = [mediaCell viewWithTag:MEDIATAG_IMAGE];
    ALAsset *asset = [imageAssetArray objectAtIndex:[imageAssetArray count] - 1 - indexPath.row];
    imageView.image = [UIImage imageWithCGImage:asset.thumbnail];
    imageView.frame = mediaCell.bounds;
    return mediaCell;
}

#pragma -ALBUM_COLLECTIONVIEW_DELEGATE

-(BOOL)collectionView:(UICollectionView *)collectionView shouldSelectItemAtIndexPath:(NSIndexPath *)indexPath{
    selectedImageIndex = (int)indexPath.row;
    return YES;
}

-(void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath{
    
}


- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Make sure your segue name in storyboard is the same as this line
    if ([[segue identifier] isEqualToString:@"PageView_Segue"]) {
        
        BCMediaPageViewController *albumViewController = [segue destinationViewController];
        albumViewController.imageAssetArray = imageAssetArray;
        albumViewController.arrayIndex = selectedImageIndex;
    }
}


#pragma -mark BCMediaReadingDelegate Methods

-(void)didAlbumReadingFinished:(NSMutableArray *)mediaArray{
    
}

-(void)didImageReadingCompleted:(UIImage *)sourceImage{
    
}

-(void)didMediaCacheReady:(NSMutableDictionary *)mediaDictionary{
    
}



#pragma -mark Button Actions

- (IBAction)exitAlbumViewController:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}

- (IBAction)startCollageCollection:(UIButton *)sender {
    
}

#pragma -mark 

-(void)generateAdd{
    __weak BCMediaCollectionViewController *self_vc = self;
    self.add_bannerView.rootViewController = self_vc;
    add_creator = [[CMAddCreator alloc] initWithBanner:self.add_bannerView];
}

-(void)dealloc{
    self.add_bannerView.rootViewController = nil;
    add_creator = nil;
}

@end
