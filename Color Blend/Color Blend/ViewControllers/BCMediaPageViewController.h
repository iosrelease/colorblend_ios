//
//  BCMediaPageViewController.h
//  RingStudio
//
//  Created by Mostafizur Rahman on 5/21/15.
//  Copyright (c) 2015 BucketCam. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <AssetsLibrary/AssetsLibrary.h>
#import "BCMediaContentViewController.h"


@interface BCMediaPageViewController : UIViewController<UIPageViewControllerDataSource, UIPageViewControllerDelegate, UIAlertViewDelegate>


@property (readwrite) NSMutableArray<ALAsset *> *imageAssetArray;


@property (readwrite) int arrayIndex;

@property (weak, nonatomic) IBOutlet UILabel *pageTitleLabel;
@property (weak, nonatomic) IBOutlet UILabel *dateTakenLabel;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *hvTopLayoutConstraint;

@property (strong, nonatomic) UIPageViewController *pageViewController;
@property (weak, nonatomic) IBOutlet UIView *headerView;


@end
