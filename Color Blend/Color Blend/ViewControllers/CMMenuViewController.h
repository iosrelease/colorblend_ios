//
//  CMMenuViewController.h
//  ColorMagic
//
//  Created by Mostafizur Rahman on 10/8/17.
//  Copyright © 2017 Mostafizur Rahman. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "CMAddCreator.h"

@interface CMMenuViewController : UIViewController
@property (strong, nonatomic) IBOutletCollection(UIButton) NSArray *buttonCollections;

@property (weak, nonatomic) IBOutlet GADBannerView *add_bannerView;
@property (strong, nonatomic) IBOutletCollection(UIButton) NSArray *buttons;

@end
