//
//  MonochromeView.h
//  Color Blend
//
//  Created by Mostafizur Rahman on 12/27/17.
//  Copyright © 2017 liilab. All rights reserved.
//

#import <GLKit/GLKit.h>

@interface MonochromeView : GLKView

@property (readonly) CGFloat maxSliderValue;
@property (readonly) CGFloat minSliderValue;


-(void)changeIntensityValue:(const CGFloat)slider_value;
-(void)setFilterColor:(UIColor *)input_color;

-(BOOL)setupDrawingContext;
-(void)setupSourceImage:(UIImage *)sourceImage;

-(UIImage *)getOutputImage;

@end
