//
//  ViewController.m
//  Color Blend
//
//  Created by Mostafizur Rahman on 12/25/17.
//  Copyright © 2017 liilab. All rights reserved.
//

#import "BlendViewController.h"
#import "BCInterfaceHelper.h"
#import "CMShareViewController.h"
#import "CMImageExtension.h"
#import "CMDialogBase.h"

@interface BlendViewController ()
<ColorSelectionDelegate,
DialogButtonDelegate>{
    
}

@end

@implementation BlendViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.colorPickerView.colorSelectionDelegate = self;
    
    
    // Do any additional setup after loading the view, typically from a nib.
}
#pragma mark - GADNativeExpressAdViewDelegate



#pragma mark - GADVideoControllerDelegate





-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    [BCInterfaceHelper setButtonLayer:self.buttonCollection];
    [self.glk_drawingView setupDrawingContext];
    [self.glk_drawingView setupSourceImage:self.sourceImage];
    self.intensitySlider.maximumValue = self.glk_drawingView.maxSliderValue;
    self.intensitySlider.minimumValue = self.glk_drawingView.minSliderValue;
    [self.view bringSubviewToFront:self.tobbarView];
    [self setupHints];
}

-(void)viewDidAppear:(BOOL)animated{
    [super viewDidAppear:animated];
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)viewDidDisappear:(BOOL)animated{
    [super viewDidDisappear:animated];
    
}
-(UIColor*) inverseColor:(UIColor *)color
{
    CGFloat r,g,b,a;
    [color getRed:&r green:&g blue:&b alpha:&a];
    return [UIColor colorWithRed:1.-r green:1.-g blue:1.-b alpha:a];
}

-(void)didSelectColor:(UIColor *)selectedColor{
    dispatch_async(dispatch_get_main_queue(), ^{
        
        self.intensitySlider.minimumTrackTintColor = selectedColor;
        self.intensitySlider.thumbTintColor = [selectedColor colorWithAlphaComponent:0.7];// [self inverseColor:selectedColor];
        [self.glk_drawingView setFilterColor:selectedColor];
    });
}
- (IBAction)changedIntensity:(UISlider *)sender {
    [self.glk_drawingView changeIntensityValue:sender.value];
}
- (IBAction)exitBlendViewController:(id)sender {
    dispatch_async(dispatch_get_main_queue(), ^{
        CMDialogBase *baseD_v = [[CMDialogBase alloc] initWithDialogType:0];
        baseD_v.transform = CGAffineTransformIdentity;
        baseD_v.frame = [[UIScreen mainScreen] bounds];
        baseD_v.hidden = YES;
        [baseD_v setTitle:@"Discarding..."];
        [baseD_v setMessageBody:@"Are you sure of your leaving? Once you leave, all the art works will be lost."];
        __weak BlendViewController *self_vc = self;
        [baseD_v setRootViewController:self_vc];
        [baseD_v setImage:[self.glk_drawingView getOutputImage]];
        [self.view addSubview:baseD_v];
        
        [BCInterfaceHelper animateOpacityViewToVisible:baseD_v anim_completion:^(BOOL finished) {
            
        }];
    });
}

-(void)clicked_YES{
    self.colorPickerView.colorSelectionDelegate = nil;
    [self.navigationController popViewControllerAnimated:YES];
}

-(void)clicked_Okay{
    
}
-(void)clicked_Cancel{
    
}

-(void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender{
    if([segue.identifier isEqualToString:@"SharingSegue"]){
        CMShareViewController *shareVC = [segue destinationViewController];
        UIImage *outptuImage = [self.glk_drawingView getOutputImage];
        shareVC.saveImageView.image = outptuImage;
        shareVC.sourceImage = outptuImage;
    }
}

-(void)setupHints{
    UILabel *label = [[UILabel alloc] initWithFrame:CGRectMake(MAINSCRN_WIDTH / 2 - 100,
                                                               _colorPickerView.frame.origin.y - 80,
                                                               200, 30)];
    label.text = @"Drag the indicator...";
    label.backgroundColor = [[UIColor whiteColor] colorWithAlphaComponent:0.75];
    label.textAlignment = NSTextAlignmentCenter;
    label.layer.borderColor = [UIColor whiteColor].CGColor;
    label.layer.borderWidth = 0.75;
    label.layer.cornerRadius = 6;
    label.layer.masksToBounds = YES;
    [[self.colorPickerView superview] addSubview:label];
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(4 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
        if(!_colorPickerView.shouldChooseColor){
            self.colorPickerView.alpha = 0.2;
        }
        [label removeFromSuperview];
    });
}

-(void)touchesBegan:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event{
    [super touchesBegan: touches withEvent:event];
    CGPoint touchPoint = [[[touches allObjects] lastObject] locationInView:self.view];
    
    if(!CGRectContainsPoint(self.colorCircleView.frame, touchPoint) ){
        
        self.tobbarView.hidden = YES;
        self.colorCircleView.hidden = YES;
    }
}

-(void)touchesEnded:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event{
    [super touchesBegan:touches withEvent:event];
    self.tobbarView.hidden = NO;
    self.colorCircleView.hidden = NO;
}
@end

