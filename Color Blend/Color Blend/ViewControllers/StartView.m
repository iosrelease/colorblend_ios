//
//  StartView.m
//  Color Blend
//
//  Created by Mostafizur Rahman on 12/30/17.
//  Copyright © 2017 liilab. All rights reserved.
//

#import "StartView.h"

@implementation StartView


// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code

    CGFloat colors [] = {
        1.0, 0.460, 0.54, 0.860,
        1.0, 0.6, 0.8, 0.860
    };
    
    CGColorSpaceRef baseSpace = CGColorSpaceCreateDeviceRGB();
    CGGradientRef gradient = CGGradientCreateWithColorComponents(baseSpace, colors, NULL, 2);
    (void)(CGColorSpaceRelease(baseSpace)), baseSpace = NULL;
    
    CGContextRef context = UIGraphicsGetCurrentContext();
    
    CGContextSaveGState(context);
    
    CGContextAddRect(context, rect);
    CGContextClip(context);
    
    CGPoint startPoint = CGPointMake(CGRectGetMidX(rect), CGRectGetMinY(rect));
    CGPoint endPoint = CGPointMake(CGRectGetMidX(rect), CGRectGetMaxY(rect));
    
    CGContextDrawLinearGradient(context, gradient, startPoint, endPoint, 0);
    (void)(CGGradientRelease(gradient)), gradient = NULL;
    
    CGContextRestoreGState(context);
    
    CGContextAddRect(context, rect);
    CGContextDrawPath(context, kCGPathStroke);
}


@end
