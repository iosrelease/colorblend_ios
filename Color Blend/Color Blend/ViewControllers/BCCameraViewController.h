//
//  BCCameraViewController.h
//  BucketCam
//
//  Created by Mostafizur Rahman on 3/29/17.
//  Copyright © 2017 Mostafizur Rahman. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <GLKit/GLKit.h>
#import "BCCameraSessionController.h"


@interface BCCameraViewController : UIViewController<CPImageCaptureDelegate>


@property (weak, nonatomic) IBOutlet UIButton *cameraCaptureButton;

@property (strong, nonatomic) IBOutletCollection(UIButton) NSArray *buttonCollections;

@end
