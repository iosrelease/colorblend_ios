//
//  CMMenuViewController.m
//  ColorMagic
//
//  Created by Mostafizur Rahman on 10/8/17.
//  Copyright © 2017 Mostafizur Rahman. All rights reserved.
//

#import "CMMenuViewController.h"
#import "BCInterfaceHelper.h"

@interface CMMenuViewController (){
    int menuLeadingConstant;
    CGFloat menuViewWidth;
    BOOL isHidden;
    BOOL firstLaunch;
    NSMutableArray *dataSourceArray;
    CMAddCreator *addCreator;
}

@end

@implementation CMMenuViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    firstLaunch = YES;
    [self initiliazeAddGroup];
    
    for(UIButton *b in self.buttons){
        b.layer.cornerRadius = 7;
        b.layer.masksToBounds = YES;
    }
    
    // Do any additional setup after loading the view.
}

-(void)dealloc{
    self.add_bannerView.rootViewController = nil;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    
    // Dispose of any resources that can be recreated.
}
-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    [BCInterfaceHelper setButtonLayer:self.buttonCollections];
    
}

-(void)viewDidDisappear:(BOOL)animated{
    [super viewDidDisappear:animated];
    
}

-(void)viewDidAppear:(BOOL)animated{
    [super viewDidAppear:animated];
    
    
}

-(void)initializeImageSlider{
    
    
}

/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */


#pragma -mark BUTTON_ACTIONS



-(void)touchesBegan:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event{
    [super touchesBegan:touches withEvent:event];
    
    
}




#pragma mark- VIDEO_TURORIAL
- (IBAction)stopAndExitVideoPreview:(UIButton *)sender {
    //    [self.videoPlayerView stopAndExitVideoPlayer];
}

- (IBAction)openVideoTutorial:(UIButton *)sender {
    NSURL *appStoreURL = [NSURL URLWithString:@"https://itunes.apple.com/us/developer/fatema-mahbub/id955066983"];
    [[UIApplication sharedApplication] openURL:appStoreURL];
    
}




#pragma -mark INIT_ADD_GROUP

-(void)initiliazeAddGroup{
    __weak CMMenuViewController *self_vc = self;
    self.add_bannerView.rootViewController = self_vc;
    addCreator = [[CMAddCreator alloc] initWithBanner:self.add_bannerView];
    
}

@end

