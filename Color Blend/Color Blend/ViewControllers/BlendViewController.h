//
//  ViewController.h
//  Color Blend
//
//  Created by Mostafizur Rahman on 12/25/17.
//  Copyright © 2017 liilab. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BCColorPickerView.h"
#import "MonochromeView.h"
#import "CMAddCreator.h"


@interface BlendViewController : UIViewController
@property (weak, nonatomic) IBOutlet BCColorPickerView *colorPickerView;

@property (weak, nonatomic) IBOutlet MonochromeView *glk_drawingView;
@property (readwrite) UIImage *sourceImage;
@property (strong, nonatomic) IBOutletCollection(UIButton) NSArray *buttonCollection;
@property (weak, nonatomic) IBOutlet UISlider *intensitySlider;
@property (weak, nonatomic) IBOutlet UIView *tobbarView;

@property (weak, nonatomic) IBOutlet UIView *colorCircleView;
@property (weak, nonatomic) IBOutlet UIButton *button;
@end

