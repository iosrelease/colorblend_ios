//
//  GradientView.m
//  Color Blend
//
//  Created by Mostafizur Rahman on 12/31/17.
//  Copyright © 2017 liilab. All rights reserved.
//

#import "GradientView.h"

@implementation GradientView


// Onlyoverride drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
    CGFloat colors [] = {
        226.0/255, 143.54/255, 173.460/255, 0.860,
        172.0/255, 153.6/255, 193.8/255, 0.860
    };
    
    CGColorSpaceRef  colorSpaceRef = CGColorSpaceCreateDeviceRGB();
    CGGradientRef gradient = CGGradientCreateWithColorComponents(colorSpaceRef, colors, NULL, 2);
    (void)(CGColorSpaceRelease(colorSpaceRef)), colorSpaceRef = NULL;
    
    CGContextRef context = UIGraphicsGetCurrentContext();
    
    
    
    CGPoint startPoint = CGPointMake(CGRectGetMidX(rect), CGRectGetMinY(rect));
    CGPoint endPoint = CGPointMake(CGRectGetMidX(rect), CGRectGetMaxY(rect));
    
    CGContextDrawLinearGradient(context, gradient, startPoint, endPoint, 0);
    (void)(CGGradientRelease(gradient)), gradient = NULL;
    
}


@end
