//
//  BCMediaContentViewController.h
//  RingStudio
//
//  Created by Mostafizur Rahman on 5/21/15.
//  Copyright (c) 2015 Dots. All rights reserved.
//

#import <UIKit/UIKit.h>


@interface BCMediaContentViewController : UIViewController

@property (readwrite) NSInteger         currentIndex;

@property (weak, nonatomic) IBOutlet UIImageView *connentImageView;
-(void)setOrginalImage:(UIImage *)image;

@end
