//
//  BCMediaCollectionViewController.h
//  BucketCam
//
//  Created by Mostafizur Rahman on 5/19/17.
//  Copyright © 2017 Mostafizur Rahman. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>
#import <AssetsLibrary/AssetsLibrary.h>
#import "CMAddCreator.h"
#import "BCDefinedConstant.h"

#import "BCInterfaceHelper.h"
#import "CMAssetReader.h"

@interface BCMediaCollectionViewController : UIViewController<UICollectionViewDelegate,
UICollectionViewDataSource,
UICollectionViewDelegateFlowLayout,
AssetReadingDelegate

>{
    
    CMAssetReader *asset_reader;
}
@property (weak, nonatomic) IBOutlet UICollectionView *albumCollectionView;


@property (weak, nonatomic) IBOutlet GADBannerView *add_bannerView;

@end
